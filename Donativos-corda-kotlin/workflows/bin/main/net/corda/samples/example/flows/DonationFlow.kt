package net.corda.samples.example.flows

import co.paralleluniverse.fibers.Suspendable
import com.r3.corda.lib.accounts.contracts.states.AccountInfo
import com.r3.corda.lib.accounts.workflows.accountService
import com.r3.corda.lib.accounts.workflows.flows.RequestKeyForAccount
import net.corda.core.contracts.Command
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.AnonymousParty
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import net.corda.samples.example.contracts.DonationContract
import net.corda.samples.example.flows.DonationFlow.Acceptor
import net.corda.samples.example.flows.DonationFlow.Initiator
import net.corda.samples.example.states.DonationState


/**
 * This flow allows two parties (the [Initiator] and the [Acceptor]) to come to an agreement about the IOU encapsulated
 * within an [DonationState].
 *
 * In our simple example, the [Acceptor] always accepts a valid IOU.
 *
 * These flows have deliberately been implemented by using only the call() method for ease of understanding. In
 * practice we would recommend splitting up the various stages of the flow into sub-routines.
 *
 * All methods called within the [FlowLogic] sub-class need to be annotated with the @Suspendable annotation.
 */
object DonationFlow {
    @InitiatingFlow
    @StartableByRPC
    class Initiator(val transactionValue: Int,
                    val issuer: String,
                    val receiver: String,
                    val description: String) : FlowLogic<SignedTransaction>() {
        /**
         * The progress tracker checkpoints each stage of the flow and outputs the specified messages when each
         * checkpoint is reached in the code. See the 'progressTracker.currentStep' expressions within the call() function.
         */
        companion object {
            object GENERATING_TRANSACTION : Step("Generating transaction based on new IOU.")
            object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
            object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
            object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
                override fun childProgressTracker() = CollectSignaturesFlow.tracker()
            }

            object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
                override fun childProgressTracker() = FinalityFlow.tracker()
            }

            fun tracker() = ProgressTracker(
                    GENERATING_TRANSACTION,
                    VERIFYING_TRANSACTION,
                    SIGNING_TRANSACTION,
                    GATHERING_SIGS,
                    FINALISING_TRANSACTION
            )
        }

        override val progressTracker = tracker()

        /**
         * The flow logic is encapsulated within the call() method.
         */
        @Suspendable
        override fun call(): SignedTransaction {


            val issuerAccount: AccountInfo = accountService.accountInfo(issuer).single().state.data
            val receiverAccount: AccountInfo = accountService.accountInfo(receiver).single().state.data

            val issuerAnonymousParty: AnonymousParty = subFlow(RequestKeyForAccount(issuerAccount))
            val receiverAnonymousParty: AnonymousParty = subFlow(RequestKeyForAccount(receiverAccount))

            val notary: Party = serviceHub.networkMapCache.notaryIdentities.single()

            val donation = DonationState(transactionValue, issuerAnonymousParty, receiverAnonymousParty, description)

            val command = Command(DonationContract.Commands.Create(), listOf(issuerAnonymousParty.owningKey, receiverAnonymousParty.owningKey))

            val txBuilder = TransactionBuilder(notary)
                .addOutputState(donation, DonationContract.ID)
                .addCommand(command)

            progressTracker.currentStep = SIGNING_TRANSACTION

            txBuilder.verify(serviceHub)

            val locallySignedTx = serviceHub.signInitialTransaction(txBuilder, listOf(ourIdentity.owningKey, issuerAnonymousParty.owningKey))

            progressTracker.currentStep = GATHERING_SIGS

            val sessionForAccountToSendTo = initiateFlow(receiverAccount.host)
            val accountToMoveToSignature = subFlow(CollectSignatureFlow(locallySignedTx, sessionForAccountToSendTo, listOf(receiverAnonymousParty.owningKey)))
            val signedByCounterParty = locallySignedTx.withAdditionalSignatures(accountToMoveToSignature)
            progressTracker.currentStep = FINALISING_TRANSACTION


            val stx = subFlow(FinalityFlow(signedByCounterParty, listOf(sessionForAccountToSendTo).filter { it.counterparty != ourIdentity }))

            return stx
        }
    }

    @InitiatedBy(Initiator::class)
    class Acceptor(val otherPartySession: FlowSession) : FlowLogic<SignedTransaction>() {
        @Suspendable
        override fun call(): SignedTransaction {
            val signTransactionFlow = object : SignTransactionFlow(otherPartySession) {
                override fun checkTransaction(stx: SignedTransaction) = requireThat {
                    //val output = stx.tx.outputs.single().data
                   // "This must be an Donation transaction." using (output is DonationState)
                    //val donation = output as DonationState
                    //"I won't accept Donations with a value inferior to 0€." using (donation.transactionValue >= 0)
                }
            }



            val txId = subFlow(signTransactionFlow).id

            return subFlow(ReceiveFinalityFlow(otherPartySession, expectedTxId = txId))
        }
    }
}

package net.corda.samples.example.flows

import co.paralleluniverse.fibers.Suspendable
import com.r3.corda.lib.accounts.contracts.states.AccountInfo
import com.r3.corda.lib.accounts.workflows.accountService
import com.r3.corda.lib.tokens.contracts.states.FungibleToken
import com.r3.corda.lib.tokens.money.EUR
import net.corda.core.flows.*
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import java.math.BigDecimal


object AccountBalanceFlow {
    @InitiatingFlow
    @StartableByRPC
    class AccountBalanceFlow(private val account: AccountInfo?) : FlowLogic<BigDecimal>() {
        /**
         * The progress tracker checkpoints each stage of the flow and outputs the specified messages when each
         * checkpoint is reached in the code. See the 'progressTracker.currentStep' expressions within the call() function.
         */
        companion object {
            object GENERATING_TRANSACTION : Step("Generating transaction based on new IOU.")
            object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
            object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
            object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
                override fun childProgressTracker() = CollectSignaturesFlow.tracker()
            }

            object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
                override fun childProgressTracker() = FinalityFlow.tracker()
            }

            fun tracker() = ProgressTracker(
                GENERATING_TRANSACTION,
                VERIFYING_TRANSACTION,
                SIGNING_TRANSACTION,
                GATHERING_SIGS,
                FINALISING_TRANSACTION
            )
        }

        override val progressTracker = tracker()

        /**
         * The flow logic is encapsulated within the call() method.
         */
        @Suspendable
        override fun call(): BigDecimal {

            val tokens = serviceHub.vaultService.queryBy<FungibleToken>(criteria = QueryCriteria.VaultQueryCriteria(status = Vault.StateStatus.UNCONSUMED)).states.filter {
                it.state.data.amount.token.tokenType == EUR && accountService.accountInfo(
                    it.state.data.holder.owningKey
                )?.state?.data?.identifier?.id == account?.identifier?.id
            }

            var tokenSum = BigDecimal("0")

            for(token in tokens){
                println(token.state.data.amount)
                tokenSum += token.state.data.amount.toDecimal()
            }
            return tokenSum
        }
    }

}



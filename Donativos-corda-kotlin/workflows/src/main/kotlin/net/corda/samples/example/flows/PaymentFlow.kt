package net.corda.samples.example.flows

import co.paralleluniverse.fibers.Suspendable
import com.r3.corda.lib.accounts.contracts.states.AccountInfo
import com.r3.corda.lib.accounts.workflows.accountService
import com.r3.corda.lib.accounts.workflows.flows.RequestKeyForAccount
import com.r3.corda.lib.tokens.contracts.utilities.of
import com.r3.corda.lib.tokens.money.EUR
import com.r3.corda.lib.tokens.workflows.flows.rpc.MoveFungibleTokens
import com.r3.corda.lib.tokens.workflows.flows.rpc.RedeemFungibleTokens
import com.r3.corda.lib.tokens.workflows.types.PartyAndAmount
import com.r3.corda.lib.tokens.workflows.utilities.heldTokenAmountCriteria
import com.r3.corda.lib.tokens.workflows.utilities.tokenAmountWithIssuerCriteria
import net.corda.core.CordaException
import net.corda.core.contracts.Command
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.AnonymousParty
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import net.corda.samples.example.contracts.PaymentContract
import net.corda.samples.example.flows.PaymentFlow.Acceptor
import net.corda.samples.example.flows.PaymentFlow.PaymentFlow
import net.corda.samples.example.states.PaymentState
import java.math.BigDecimal


/**
 * This flow allows two parties (the [PaymentFlow] and the [Acceptor]) to come to an agreement about the IOU encapsulated
 * within an [PaymentState].
 *
 * In our simple example, the [Acceptor] always accepts a valid IOU.
 *
 * These flows have deliberately been implemented by using only the call() method for ease of understanding. In
 * practice we would recommend splitting up the various stages of the flow into sub-routines.
 *
 * All methods called within the [FlowLogic] sub-class need to be annotated with the @Suspendable annotation.
 */
object PaymentFlow {
    @InitiatingFlow
    @StartableByRPC
    class PaymentFlow(
        private val transactionValue: Int,
        private val issuer: String,
        private val receiver: String,
        private val institution: String,
        private val cause: String,
        private val description: String
    ) : FlowLogic<SignedTransaction>() {
        /**
         * The progress tracker checkpoints each stage of the flow and outputs the specified messages when each
         * checkpoint is reached in the code. See the 'progressTracker.currentStep' expressions within the call() function.
         */
        companion object {
            object GENERATING_TRANSACTION : Step("Generating transaction based on new IOU.")
            object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
            object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
            object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
                override fun childProgressTracker() = CollectSignaturesFlow.tracker()
            }

            object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
                override fun childProgressTracker() = FinalityFlow.tracker()
            }

            fun tracker() = ProgressTracker(
                GENERATING_TRANSACTION,
                VERIFYING_TRANSACTION,
                SIGNING_TRANSACTION,
                GATHERING_SIGS,
                FINALISING_TRANSACTION
            )
        }

        override val progressTracker = tracker()

        /**
         * The flow logic is encapsulated within the call() method.
         */
        @Suspendable
        override fun call(): SignedTransaction {


            /* if (subFlow(AccountBalanceFlow.AccountBalanceFlow(issuer)) < BigDecimal(transactionValue)) {
                throw CordaException("Not enough available Balance")
            } */

            val issuerAccount: AccountInfo = accountService.accountInfo(issuer).single().state.data
            val receiverAccount: AccountInfo = accountService.accountInfo(receiver).single().state.data

            val issuerAnonymousParty: AnonymousParty = subFlow(RequestKeyForAccount(issuerAccount))
            val receiverAnonymousParty: AnonymousParty = subFlow(RequestKeyForAccount(receiverAccount))

            /* subFlow(
                MoveFungibleTokens(
                    partiesAndAmounts = listOf(PartyAndAmount(receiverAnonymousParty, transactionValue of EUR)),
                    queryCriteria = tokenAmountWithIssuerCriteria(EUR, ourIdentity).and(
                        heldTokenAmountCriteria(
                            EUR,
                            issuerAnonymousParty
                        )
                    ),
                    changeHolder = issuerAnonymousParty
                )
            )
            subFlow(
                RedeemFungibleTokens(
                    amount = transactionValue of EUR,
                    issuer = ourIdentity,
                    changeHolder = receiverAnonymousParty,
                    queryCriteria = heldTokenAmountCriteria(EUR, receiverAnonymousParty)
                )
            ) */

            val notary: Party = serviceHub.networkMapCache.notaryIdentities.single()

            val payment = PaymentState(transactionValue, issuerAnonymousParty, receiverAnonymousParty, institution, cause, description)

            val command = Command(
                PaymentContract.Commands.Create(),
                listOf(issuerAnonymousParty.owningKey, receiverAnonymousParty.owningKey)
            )

            val txBuilder = TransactionBuilder(notary)
                .addOutputState(payment, PaymentContract.ID)
                .addCommand(command)

            progressTracker.currentStep = SIGNING_TRANSACTION

            txBuilder.verify(serviceHub)

            val locallySignedTx = serviceHub.signInitialTransaction(
                txBuilder,
                listOf(ourIdentity.owningKey, issuerAnonymousParty.owningKey)
            )

            progressTracker.currentStep = GATHERING_SIGS

            val sessionForAccountToSendTo = initiateFlow(receiverAccount.host)
            val accountToMoveToSignature = subFlow(
                CollectSignatureFlow(
                    locallySignedTx,
                    sessionForAccountToSendTo,
                    listOf(receiverAnonymousParty.owningKey)
                )
            )
            val signedByCounterParty = locallySignedTx.withAdditionalSignatures(accountToMoveToSignature)
            progressTracker.currentStep = FINALISING_TRANSACTION


            val stx = subFlow(
                FinalityFlow(
                    signedByCounterParty,
                    listOf(sessionForAccountToSendTo).filter { it.counterparty != ourIdentity })
            )

            return stx
        }
    }

    @InitiatedBy(PaymentFlow::class)
    class Acceptor(val otherPartySession: FlowSession) : FlowLogic<SignedTransaction>() {
        @Suspendable
        override fun call(): SignedTransaction {
            val signTransactionFlow = object : SignTransactionFlow(otherPartySession) {
                override fun checkTransaction(stx: SignedTransaction) = requireThat {
                    //val output = stx.tx.outputs.single().data
                    // "This must be an Donation transaction." using (output is DonationState)
                    //val donation = output as DonationState
                    //"I won't accept Donations with a value inferior to 0€." using (donation.transactionValue >= 0)
                }
            }


            val txId = subFlow(signTransactionFlow).id

            return subFlow(ReceiveFinalityFlow(otherPartySession, expectedTxId = txId))
        }
    }
}

package net.corda.samples.example.webserver

import com.r3.corda.lib.accounts.workflows.flows.CreateAccount
import net.corda.core.contracts.StateAndRef
import com.google.gson.Gson
import net.corda.core.identity.CordaX500Name
import net.corda.core.messaging.startTrackedFlow
import net.corda.core.messaging.vaultQueryBy
import net.corda.core.utilities.getOrThrow
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import javax.servlet.http.HttpServletRequest
import net.corda.client.jackson.JacksonSupport
import net.corda.core.identity.Party
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.samples.example.flows.AccountInfoFlow
import net.corda.samples.example.flows.AccountInfoFlow.GetAccountFromKeyFlow
import net.corda.samples.example.flows.AccountInfoFlow.GetAccountInfoFlow
import net.corda.samples.example.flows.DonationFlow
import net.corda.samples.example.flows.DonationFlow.Initiator
import net.corda.samples.example.states.DonationState
import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x500.style.BCStyle
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.bind.annotation.*
import java.security.PublicKey
import javax.swing.plaf.nimbus.State

val SERVICE_NAMES = listOf("Notary", "Network Map Service")

/**
 * Define your API endpoints here.
 */
@RestController
@RequestMapping("/") // The paths for HTTP requests are relative to this base path.
class Controller(rpc: NodeRPCConnection) {


    companion object {
        private val logger = LoggerFactory.getLogger(RestController::class.java)
    }
    @Bean
    open fun mappingJackson2HttpMessageConverter(@Autowired rpcConnection: NodeRPCConnection): MappingJackson2HttpMessageConverter {
        val mapper = JacksonSupport.createDefaultMapper(rpcConnection.proxy)
        val converter = MappingJackson2HttpMessageConverter()
        converter.objectMapper = mapper
        return converter
    }


    private val myLegalName = rpc.proxy.nodeInfo().legalIdentities.first().name
    private val proxy = rpc.proxy


     /** Initiates a flow to agree an IOU between two parties.
     *
     * Once the flow finishes it will have written the IOU to ledger. Both the lender and the borrower will be able to
     * see it when calling /spring/api/ious on their respective nodes.
     *
     * This end-point takes a Party name parameter as part of the path. If the serving node can't find the other party
     * in its network map cache, it will return an HTTP bad request.
     *
     * The flow is invoked asynchronously. It returns a future when the flow's call() method returns.
     */

    @PostMapping(value = ["create-donation"], produces = [MediaType.TEXT_PLAIN_VALUE], headers = ["Content-Type=application/x-www-form-urlencoded"])
    fun createIOU(request: HttpServletRequest): ResponseEntity<String> {
        val donationValue = request.getParameter("value").toInt()
        val issuerName = request.getParameter("issuer")
        val receiverName = request.getParameter("receiver")
        val description = request.getParameter("description")
        if(issuerName == null){
            return ResponseEntity.badRequest().body("Query parameter 'issuer' must not be null.\n")
        }
        if(receiverName == null){
            return ResponseEntity.badRequest().body("Query parameter 'receiver' must not be null.\n")
        }
        if (donationValue <= 0 ) {
            return ResponseEntity.badRequest().body("Query parameter 'value' must be non-negative.\n")
        }
        if(description == null){
            return ResponseEntity.badRequest().body("Query parameter 'description' must not be null.\n")
        }


        return try {
            val signedTx = proxy.startTrackedFlow(::Initiator, donationValue, issuerName, receiverName, description).returnValue.getOrThrow()
            ResponseEntity.status(HttpStatus.CREATED).body("Transaction id ${signedTx.id} committed to ledger.\n")

        } catch (ex: Throwable) {
            logger.error(ex.message, ex)
            ResponseEntity.badRequest().body(ex.message!!)
        }
    }

    /**
     * Displays all IOU states that only this node has been involved in.
     */
    @GetMapping(value = ["my-donations"], params = ["name"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getMyDonations(@RequestParam(value="name") issuerName: String): ResponseEntity<List<StateAndRef<DonationState>>> {

        val issuer = proxy.startTrackedFlow(::GetAccountInfoFlow, issuerName).returnValue.get()
        val myDonations: List<StateAndRef<DonationState>> = proxy.vaultQueryByCriteria(
            criteria = QueryCriteria.VaultQueryCriteria(externalIds = listOf(issuer.identifier.id)),
            contractStateType = DonationState::class.java
        ).states

        return ResponseEntity.ok(myDonations)
    }
    @PostMapping(value = ["create-account"], produces = [MediaType.TEXT_PLAIN_VALUE], headers = ["Content-Type=application/x-www-form-urlencoded"])
    fun createAccount(request: HttpServletRequest) : ResponseEntity<String>{
        val accountName = request.getParameter("account-name")

        if(accountName == null){
            return ResponseEntity.badRequest().body("Query parameter 'account-name' must not be null.\n")
        }

        return try {
            val account = proxy.startTrackedFlow(::CreateAccount, accountName).returnValue.getOrThrow()
            ResponseEntity.status(HttpStatus.CREATED).body("Account with id ${account.state.data.identifier} created");
        } catch (ex: Throwable){
            logger.error(ex.message, ex)
            ResponseEntity.badRequest().body(ex.message!!)
        }
    }

    /*@GetMapping(value = ["uuid-from-Key"], params = ["key"], produces = [MediaType.TEXT_PLAIN_VALUE])
    fun getUUIDFromKey(@RequestParam(value = "key") accountKey : PublicKey) : ResponseEntity<String>{

        return ResponseEntity.ok(proxy.startTrackedFlow(::GetAccountFromKeyFlow, accountKey).returnValue.get())
    }*/
}

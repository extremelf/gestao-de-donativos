package net.corda.samples.example.webserver

import com.google.gson.JsonObject
import com.r3.corda.lib.accounts.workflows.flows.CreateAccount
import com.r3.corda.lib.tokens.money.EUR
import net.corda.client.jackson.JacksonSupport
import net.corda.core.contracts.StateAndRef
import net.corda.core.messaging.startTrackedFlow
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.utilities.getOrThrow
import net.corda.samples.example.flows.AccountBalanceFlow.AccountBalanceFlow
import net.corda.samples.example.flows.AccountInfoFlow.GetAccountInfoByAnonymousPartyFlow
import net.corda.samples.example.flows.AccountInfoFlow.GetAccountInfoByNameFlow
import net.corda.samples.example.flows.AccountInfoFlow.GetAccountInfoByUUIDFlow
import net.corda.samples.example.flows.DonationFlow.DonationFlow
import net.corda.samples.example.flows.PaymentFlow.PaymentFlow
import net.corda.samples.example.states.DonationState
import net.corda.samples.example.states.PaymentState
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest

val SERVICE_NAMES = listOf("Notary", "Network Map Service")

/**
 * Define your API endpoints here.
 */
@RestController
@RequestMapping("/") // The paths for HTTP requests are relative to this base path.
class Controller(rpc: NodeRPCConnection) {


    companion object {
        private val logger = LoggerFactory.getLogger(RestController::class.java)
    }

    @Bean
    fun mappingJackson2HttpMessageConverter(@Autowired rpcConnection: NodeRPCConnection): MappingJackson2HttpMessageConverter {
        val mapper = JacksonSupport.createDefaultMapper(rpcConnection.proxy)
        val converter = MappingJackson2HttpMessageConverter()
        converter.objectMapper = mapper
        return converter
    }


    private val myLegalName = rpc.proxy.nodeInfo().legalIdentities.first().name
    private val proxy = rpc.proxy


    /** Initiates a flow to agree an IOU between two parties.
     *
     * Once the flow finishes it will have written the IOU to ledger. Both the lender and the borrower will be able to
     * see it when calling /spring/api/ious on their respective nodes.
     *
     * This end-point takes a Party name parameter as part of the path. If the serving node can't find the other party
     * in its network map cache, it will return an HTTP bad request.
     *
     * The flow is invoked asynchronously. It returns a future when the flow's call() method returns.
     */

    @PostMapping(
        value = ["create-donation"],
        produces = [MediaType.TEXT_PLAIN_VALUE],
        headers = ["Content-Type=application/x-www-form-urlencoded"]
    )
    fun createDonation(request: HttpServletRequest): ResponseEntity<String> {
        val donationValue = request.getParameter("value").toInt()
        val issuerName = request.getParameter("issuer")
        val receiverName = request.getParameter("receiver")
        val institution = request.getParameter("institution")
        val cause = request.getParameter("cause")
        val description = request.getParameter("description")

        if (issuerName == null) {
            return ResponseEntity.badRequest().body("Query parameter 'issuer' must not be null.\n")
        }
        if (receiverName == null) {
            return ResponseEntity.badRequest().body("Query parameter 'receiver' must not be null.\n")
        }
        if (donationValue <= 0) {
            return ResponseEntity.badRequest().body("Query parameter 'value' must be non-negative.\n")
        }
        if (description == null) {
            return ResponseEntity.badRequest().body("Query parameter 'description' must not be null.\n")
        }


        return try {
            val signedTx = proxy.startTrackedFlow(
                ::DonationFlow,
                donationValue,
                issuerName,
                receiverName,
                institution,
                cause,
                description
            ).returnValue.getOrThrow()
            ResponseEntity.status(HttpStatus.CREATED).body("Transaction id ${signedTx.id} committed to ledger.\n")

        } catch (ex: Throwable) {
            logger.error(ex.message, ex)
            ResponseEntity.badRequest().body(ex.message!!)
        }
    }

    @PostMapping(
        value = ["create-payment"],
        produces = [MediaType.TEXT_PLAIN_VALUE],
        headers = ["Content-Type=application/x-www-form-urlencoded"]
    )
    fun createPayment(request: HttpServletRequest): ResponseEntity<String> {
        val paymentValue = request.getParameter("value").toInt()
        val issuerName = request.getParameter("issuer")
        val receiverName = request.getParameter("receiver")
        val institution = request.getParameter("institution")
        val cause = request.getParameter("cause")
        val description = request.getParameter("description")

        if (issuerName == null) {
            return ResponseEntity.badRequest().body("Query parameter 'issuer' must not be null.\n")
        }
        if (receiverName == null) {
            return ResponseEntity.badRequest().body("Query parameter 'receiver' must not be null.\n")
        }
        if (paymentValue <= 0) {
            return ResponseEntity.badRequest().body("Query parameter 'value' must be non-negative.\n")
        }
        if (description == null) {
            return ResponseEntity.badRequest().body("Query parameter 'description' must not be null.\n")
        }


        return try {
            val signedTx = proxy.startTrackedFlow(
                ::PaymentFlow,
                paymentValue,
                issuerName,
                receiverName,
                institution,
                cause,
                description
            ).returnValue.getOrThrow()
            ResponseEntity.status(HttpStatus.CREATED).body("Transaction id ${signedTx.id} committed to ledger.\n")

        } catch (ex: Throwable) {
            logger.error(ex.message, ex)
            ResponseEntity.badRequest().body(ex.message!!)
        }
    }

    /**
     * Displays all Donation states that only this account has been the issuer.
     */
    @GetMapping(value = ["my-donations"], params = ["uuid"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getMyDonations(@RequestParam(value = "uuid") issuerUUIDString: String): ResponseEntity<MutableList<JsonObject>> {
        val jsonList: MutableList<JsonObject> = mutableListOf()
        val issuer = proxy.startTrackedFlow(::GetAccountInfoByNameFlow, issuerUUIDString).returnValue.get()
        val myDonations: List<StateAndRef<DonationState>> = proxy.vaultQueryByCriteria(
            criteria = QueryCriteria.VaultQueryCriteria(externalIds = listOf(issuer?.identifier!!.id)),
            contractStateType = DonationState::class.java
        ).states.filter {
            proxy.startTrackedFlow(
                ::GetAccountInfoByAnonymousPartyFlow,
                it.state.data.issuer
            ).returnValue.get()?.identifier?.id == issuer.identifier.id
        }

        for (donation in myDonations) {
            val donationjson = JsonObject()
            donationjson.addProperty(
                "issuer",
                proxy.startTrackedFlow(
                    ::GetAccountInfoByAnonymousPartyFlow,
                    donation.state.data.issuer
                ).returnValue.get()?.identifier?.id.toString()
            )
            donationjson.addProperty(
                "receiver",
                proxy.startTrackedFlow(
                    ::GetAccountInfoByAnonymousPartyFlow,
                    donation.state.data.receiver
                ).returnValue.get()?.identifier?.id.toString()
            )
            donationjson.addProperty("institution", donation.state.data.institution)
            donationjson.addProperty("cause", donation.state.data.cause)
            donationjson.addProperty("value", donation.state.data.transactionValue)
            donationjson.addProperty("description", donation.state.data.description)
            donationjson.addProperty("date", donation.state.data.data.toString())
            jsonList.add(donationjson)
        }
        return ResponseEntity.ok(jsonList)
    }

    /**
     * Displays all Payment states that only this account has been the issuer.
     */
    @GetMapping(value = ["my-payments"], params = ["uuid"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getMyPayments(@RequestParam(value = "uuid") issuerUUIDString: String): ResponseEntity<MutableList<JsonObject>> {
        val jsonList: MutableList<JsonObject> = mutableListOf()
        val issuer = proxy.startTrackedFlow(::GetAccountInfoByNameFlow, issuerUUIDString).returnValue.get()
        val myPayments: List<StateAndRef<PaymentState>> = proxy.vaultQueryByCriteria(
            criteria = QueryCriteria.VaultQueryCriteria(externalIds = listOf(issuer?.identifier!!.id)),
            contractStateType = PaymentState::class.java
        ).states.filter {
            proxy.startTrackedFlow(
                ::GetAccountInfoByAnonymousPartyFlow,
                it.state.data.issuer
            ).returnValue.get()?.identifier?.id == issuer.identifier.id
        }

        for (payment in myPayments) {
            val paymentJson = JsonObject()
            paymentJson.addProperty(
                "issuer",
                proxy.startTrackedFlow(
                    ::GetAccountInfoByAnonymousPartyFlow,
                    payment.state.data.issuer
                ).returnValue.get()?.identifier?.id.toString()
            )
            paymentJson.addProperty(
                "receiver",
                proxy.startTrackedFlow(
                    ::GetAccountInfoByAnonymousPartyFlow,
                    payment.state.data.receiver
                ).returnValue.get()?.identifier?.id.toString()
            )
            paymentJson.addProperty("institution", payment.state.data.institution)
            paymentJson.addProperty("cause", payment.state.data.cause)
            paymentJson.addProperty("value", payment.state.data.transactionValue)
            paymentJson.addProperty("description", payment.state.data.description)
            paymentJson.addProperty("date", payment.state.data.data.toString())
            jsonList.add(paymentJson)
        }
        return ResponseEntity.ok(jsonList)
    }

    /**
     * Displays all Donation states that only this account has been the receiver.
     */
    @GetMapping(value = ["received-donations"], params = ["uuid"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getReceivedDonations(@RequestParam(value = "uuid") receiverString: String): ResponseEntity<MutableList<JsonObject>> {
        val jsonList: MutableList<JsonObject> = mutableListOf()
        val receiver = proxy.startTrackedFlow(::GetAccountInfoByNameFlow, receiverString).returnValue.get()
        val myDonations: List<StateAndRef<DonationState>> = proxy.vaultQueryByCriteria(
            criteria = QueryCriteria.VaultQueryCriteria(externalIds = listOf(receiver?.identifier!!.id)),
            contractStateType = DonationState::class.java
        ).states.filter {
            proxy.startTrackedFlow(
                ::GetAccountInfoByAnonymousPartyFlow,
                it.state.data.receiver
            ).returnValue.get()?.identifier?.id == receiver.identifier.id
        }

        for (donation in myDonations) {
            val donationjson = JsonObject()
            donationjson.addProperty(
                "issuer",
                proxy.startTrackedFlow(
                    ::GetAccountInfoByAnonymousPartyFlow,
                    donation.state.data.issuer
                ).returnValue.get()?.identifier?.id.toString()
            )
            donationjson.addProperty(
                "receiver",
                proxy.startTrackedFlow(
                    ::GetAccountInfoByAnonymousPartyFlow,
                    donation.state.data.receiver
                ).returnValue.get()?.identifier?.id.toString()
            )
            donationjson.addProperty("value", donation.state.data.transactionValue)
            donationjson.addProperty("institution", donation.state.data.institution)
            donationjson.addProperty("cause", donation.state.data.cause)
            donationjson.addProperty("description", donation.state.data.description)
            donationjson.addProperty("date", donation.state.data.data.toString())
            jsonList.add(donationjson)
        }
        return ResponseEntity.ok(jsonList)
    }

    /**
     * Displays all Payment states that only this account has been the receiver.
     */
    @GetMapping(value = ["received-payments"], params = ["uuid"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getReceivedPayments(@RequestParam(value = "uuid") receiverUUIDString: String): ResponseEntity<MutableList<JsonObject>> {
        val jsonList: MutableList<JsonObject> = mutableListOf()
        val receiver = proxy.startTrackedFlow(::GetAccountInfoByNameFlow, receiverUUIDString).returnValue.get()
        val myPayments: List<StateAndRef<PaymentState>> = proxy.vaultQueryByCriteria(
            criteria = QueryCriteria.VaultQueryCriteria(externalIds = listOf(receiver?.identifier!!.id)),
            contractStateType = PaymentState::class.java
        ).states.filter {
            proxy.startTrackedFlow(
                ::GetAccountInfoByAnonymousPartyFlow,
                it.state.data.receiver
            ).returnValue.get()?.identifier?.id == receiver.identifier.id
        }

        for (payment in myPayments) {
            val paymentJson = JsonObject()
            paymentJson.addProperty(
                "issuer",
                proxy.startTrackedFlow(
                    ::GetAccountInfoByAnonymousPartyFlow,
                    payment.state.data.issuer
                ).returnValue.get()?.identifier?.id.toString()
            )
            paymentJson.addProperty(
                "receiver",
                proxy.startTrackedFlow(
                    ::GetAccountInfoByAnonymousPartyFlow,
                    payment.state.data.receiver
                ).returnValue.get()?.identifier?.id.toString()
            )
            paymentJson.addProperty("value", payment.state.data.transactionValue)
            paymentJson.addProperty("institution", payment.state.data.institution)
            paymentJson.addProperty("cause", payment.state.data.cause)
            paymentJson.addProperty("description", payment.state.data.description)
            paymentJson.addProperty("date", payment.state.data.data.toString())
            jsonList.add(paymentJson)
        }
        return ResponseEntity.ok(jsonList)
    }

    /**
     * Creates an account in the node with the specified name and returns the uuid.
     */
    @PostMapping(
        value = ["create-account"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        headers = ["Content-Type=application/x-www-form-urlencoded"]
    )
    fun createAccount(request: HttpServletRequest): ResponseEntity<String> {
        val accountEmail = request.getParameter("email")

        if (accountEmail == null) {
            return ResponseEntity.badRequest().body("Query parameter 'account-name' must not be null.\n")
        }

        return try {
            val account = proxy.startTrackedFlow(::CreateAccount, accountEmail).returnValue.getOrThrow()
            ResponseEntity.status(HttpStatus.CREATED).body("{\"uuid\": \"${account.state.data.identifier}\"}")
        } catch (ex: Throwable) {
            logger.error(ex.message, ex)
            ResponseEntity.badRequest().body("erro")
        }
    }


    /**
     * Displays the amount of tokens the account has.
     */
    @GetMapping(
        value = ["balance"],
        params = ["uuid"],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun getBalance(@RequestParam(value = "uuid") accountUUIDString: String): ResponseEntity<JsonObject> {
        val accountUUID = UUID.fromString(accountUUIDString)
        val accountInfo = proxy.startTrackedFlow(::GetAccountInfoByUUIDFlow, accountUUID).returnValue.getOrThrow()
        val tokenAmount = proxy.startTrackedFlow(::AccountBalanceFlow, accountInfo).returnValue.getOrThrow()
        val tokenJson = JsonObject()
        tokenJson.addProperty("value", tokenAmount)
        tokenJson.addProperty("currency", EUR.tokenIdentifier)

        return ResponseEntity.ok(tokenJson)
    }
}

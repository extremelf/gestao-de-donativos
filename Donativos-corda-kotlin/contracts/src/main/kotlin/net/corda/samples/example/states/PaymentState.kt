package net.corda.samples.example.states

import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.AnonymousParty
import net.corda.samples.example.contracts.PaymentContract
import java.util.*

/**
 * The state object recording IOU agreements between two parties.
 *
 * A state must implement [ContractState] or one of its descendants.
 *
 * @param transactionValue the value of the Donation.
 * @param issuer the party issuing the Donation.
 * @param receiver the party receiving and approving the Donation.
 * @param description description of the Donation
 */
@BelongsToContract(PaymentContract::class)
data class PaymentState(val transactionValue: Int,
                        val issuer: AnonymousParty,
                        val receiver: AnonymousParty,
                        val institution: String,
                        val cause: String,
                        val description: String,
                        val data: Date = Date()):
        ContractState {
    /** The public keys of the involved parties. */
    override val participants: List<AbstractParty> get() = listOf(issuer, receiver)

}

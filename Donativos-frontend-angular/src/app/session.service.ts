import { Injectable } from '@angular/core';
import SuperTokens from 'supertokens-website';

SuperTokens.init({ apiDomain: 'http://localhost:3001', apiBasePath: '/auth' });
@Injectable({
  providedIn: 'root',
})
export class SessionService {
  public accessToken: any;
  constructor() {}

  async refreshAccessToken() {
    this.accessToken = await SuperTokens.getAccessTokenPayloadSecurely();
  }

  async doesSessionExist(){
    return await SuperTokens.doesSessionExist()
  }
}

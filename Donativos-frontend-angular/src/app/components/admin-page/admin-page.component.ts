import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SessionService } from 'src/app/session.service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css'],
})
export class AdminPageComponent implements OnInit {
  public pendingInstitutions: any[] = [];
  public pendingCompanies: any[] = [];
  public accessTokenPayload: any;

  constructor(private fb: FormBuilder, private sessionServ: SessionService) {}
  getPendingInstitutions() {
    fetch('http://localhost:3001/pending-institutions')
      .then((data) => data.json())
      .then((data) => (this.pendingInstitutions = data));
  }

  getPendingCompanies() {
    fetch('http://localhost:3001/pending-companies')
      .then((data) => data.json())
      .then((data) => (this.pendingCompanies = data));
  }

  acceptInstitution(form: any) {
    fetch('http://localhost:3001/accept-institution', {
      method: 'PUT',
      body: new URLSearchParams({
        institutionId: form.value.institutionId,
      }),
    }).then(() => this.getPendingInstitutions());
  }

  rejectInstitution(form: any) {
    if (this.accessTokenPayload.isAdmin) {
      fetch('http://localhost:3001/reject-institution', {
        method: 'DELETE',
        body: new URLSearchParams({
          user_id: form.value.userId,
          institution_id: form.value.institutionId,
        }),
      }).then(() => this.getPendingInstitutions());
    }
  }

  acceptCompany(form: any) {
    fetch('http://localhost:3001/accept-company', {
      method: 'PUT',
      body: new URLSearchParams({
        companyId: form.value.companyId,
      }),
    });
    this.getPendingCompanies();
  }

  rejectCompany(form: any) {
    if (this.accessTokenPayload.isAdmin) {
      fetch('http://localhost:3001/reject-company', {
        method: 'DELETE',
        body: new URLSearchParams({
          user_id: form.value.userId,
          company_id: form.value.companyId,
        }),
      }).then(() => this.getPendingCompanies());
    }
  }

  async ngOnInit(): Promise<void> {
    await this.sessionServ.refreshAccessToken();
    this.accessTokenPayload = this.sessionServ.accessToken;
    this.getPendingInstitutions();
    this.getPendingCompanies();
  }
}

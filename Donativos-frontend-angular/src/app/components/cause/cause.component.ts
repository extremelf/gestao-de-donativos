import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-cause',
  templateUrl: './cause.component.html',
  styleUrls: ['./cause.component.css'],
})
export class CauseComponent implements OnInit {
  public id: any;
  public cause: any;
  public donations: any[] = []
  public payments: any[] = []
  public combinedArray: any[] = [];
  constructor(private route: ActivatedRoute) {}

  getCause(){
    fetch(`http://localhost:3001/get-cause?causeId=${this.id}`)
    .then(data => data.json())
    .then(data => this.cause = data)
  }

  async getDonations(){
    await fetch(`http://localhost:3001/received-cause-donation?causeId=${this.id}`)
    .then(data => data.json())
    .then(data => this.donations = data)
  }

  async getPayments(){
    await fetch(`http://localhost:3001/received-cause-payment?causeId=${this.id}`)
    .then(data => data.json())
    .then(data => this.payments = data)
    }

  async ngOnInit(): Promise<void> {
    this.route.params.subscribe(async data => {
      this.id = data['id']
      this.getCause()
      this.getDonations()
      .then(() => {
        this.getPayments()
        .then(() =>{
          let mergedArrays = this.donations.concat(this.payments)
          this.combinedArray = mergedArrays.slice().sort((a,b) => {return Date.parse(a.date+"T"+a.time) - Date.parse(b.date+"T"+b.time)})
          this.combinedArray = this.combinedArray.reverse()
        })      
      })
      
    })
  }
  displayedColumns: string[] = ['Name', 'Type', 'Description', 'Date', 'Time', 'Value'];
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SessionService } from 'src/app/session.service';

@Component({
  selector: 'app-donation',
  templateUrl: './donation.component.html',
  styleUrls: ['./donation.component.css'],
})
export class DonationComponent implements OnInit {
  public institutions: any;
  public causes: any;
  public donationForm: FormGroup | undefined;
  public donated: boolean = false;
  public accessTokenPayload: any;

  constructor(private fb: FormBuilder, private sessionServ: SessionService) {}
  getInstitutions() {
    fetch('http://localhost:3001/get-valid-institutions')
      .then((data) => data.json())
      .then((data) => (this.institutions = data));
  }

  getCausesFromInstitution(institutionId: any) {
    if (!institutionId.match('Select institution')) {
      fetch(`http://localhost:3001/get-causes?institutionId=${institutionId}`)
        .then((data) => data.json())
        .then((data) => (this.causes = data));
    } else {
      this.causes = null;
    }
  }

  submitDonation() {
    let value = this.donationForm?.value;
    if(!this.donationForm?.controls['anonCheck'].value){
      fetch('http://localhost:3001/create-donation', {
        method: 'POST',
        body: new URLSearchParams({
          receiver: value.institution,
          amount: value.amount,
          cause: value.cause,
          description: value.message,
        }),
      });
    } else {
      fetch('http://localhost:3001/create-anon-donation', {
        method: 'POST',
        body: new URLSearchParams({
          receiver: value.institution,
          amount: value.amount,
          cause: value.cause,
          description: value.message,
        }),
      });
    }
    
    
    this.donated = true;
  }

  get email() {
    return this.donationForm?.get('email');
  }
  get amount() {
    return this.donationForm?.get('amount');
  }
  get institution() {
    return this.donationForm?.get('institution');
  }
  get cause() {
    return this.donationForm?.get('cause');
  }
  get message() {
    return this.donationForm?.get('message');
  }
  get cardName() {
    return this.donationForm?.get('cardName');
  }
  get cardNumber() {
    return this.donationForm?.get('cardNumber');
  }
  get expireDate() {
    return this.donationForm?.get('expireDate');
  }
  get ccv() {
    return this.donationForm?.get('ccv');
  }

  async ngOnInit(): Promise<void> {
    await this.getInstitutions();
    if(await this.sessionServ.doesSessionExist()){
      await this.sessionServ.refreshAccessToken()
      this.accessTokenPayload = this.sessionServ.accessToken;  
    }
    this.donationForm = this.fb.group({
      email: [await this.sessionServ.doesSessionExist() ? this.accessTokenPayload.email : '', [Validators.required, Validators.email]],
      amount: ['', [Validators.required]],
      institution: ['Select institution', [Validators.required]],
      cause: [''],
      anonCheck: ['',[Validators.required]],
      message: ['', [Validators.maxLength(200)]],
      cardName: ['', [Validators.required]],
      cardNumber: ['', [Validators.required]],
      expireDate: ['', [Validators.required]],
      ccv: ['', [Validators.required, Validators.maxLength(3)]],
    });
    this.donationForm
      .get('institution')
      ?.valueChanges.subscribe((data) => this.getCausesFromInstitution(data));
  }
}

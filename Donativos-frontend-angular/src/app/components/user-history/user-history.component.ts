import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-history',
  templateUrl: './user-history.component.html',
  styleUrls: ['./user-history.component.css'],
})
export class UserHistoryComponent implements OnInit {
  @Input()
  public donations: any[] = []
  constructor() {}
  

  ngOnInit(): void {
  }
}

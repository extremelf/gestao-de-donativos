import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/session.service';

import SuperTokens from 'supertokens-website';

SuperTokens.init({ apiDomain: 'http://localhost:3001', apiBasePath: '/auth' });
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
})
export class NavComponent implements OnInit {
  public isAuth: boolean = false
  public accessTokenPayload: any = "";
  constructor(private sessionServ: SessionService) {
    
  }
  async isLoggedIn(){
    if(await SuperTokens.doesSessionExist()){
      this.isAuth = true
    } else {
      this.isAuth = false
    }
  }

  login() {
    window.location.href = 'http://localhost:3000/auth';
  }

  async logout() {
    await SuperTokens.signOut();
    window.location.href = '/';
    await this.isLoggedIn()
  }

  async ngOnInit(): Promise<void> {
    await this.isLoggedIn()
    if(await this.sessionServ.doesSessionExist()){
      await this.sessionServ.refreshAccessToken();
      this.accessTokenPayload = this.sessionServ.accessToken;
    }
  }

}

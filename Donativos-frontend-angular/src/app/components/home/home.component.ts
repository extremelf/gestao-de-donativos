import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/session.service';
import SuperTokens from 'supertokens-website';

SuperTokens.init({ apiDomain: 'http://localhost:3001', apiBasePath: '/auth' });

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public institutions: any[] = [];
  public causes: any[] = [];
  public isAuth: boolean = false;
  public accessTokenPayload: any | undefined;
  constructor(private sessionServ: SessionService) {}

  getInstitutions() {
    fetch('http://localhost:3001/get-valid-institutions')
      .then((data) => data.json())
      .then((data) => (this.institutions = data));
  }

  getCauses(){
    fetch('http://localhost:3001/get-all-causes')
    .then(data => data.json())
    .then(data => this.causes = data)
  }

  async isLoggedIn(){
    if(await SuperTokens.doesSessionExist()){
      this.isAuth = true
    } else {
      this.isAuth = false
    }
  }

  login(){
    window.location.href = "http://localhost:3000/auth"
  }
  async ngOnInit(): Promise<void> {
    if(await SuperTokens.doesSessionExist()){
      await this.sessionServ.refreshAccessToken();
      this.accessTokenPayload = this.sessionServ.accessToken;
    }
    await this.getInstitutions();
    await this.getCauses();
    await this.isLoggedIn()
    
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SessionService } from 'src/app/session.service';

@Component({
  selector: 'app-company-managment',
  templateUrl: './company-managment.component.html',
  styleUrls: ['./company-managment.component.css'],
})
export class CompanyManagmentComponent implements OnInit {
  public accessTokenPayload: any;
  public companyInfo: any;
  public pendingUsers: any[] = [];
  public pendingDonations: any[] = [];
  public institutions: any[] = [];
  public donations: any[] = [];
  public payments: any[] = [];
  public combinedArray: any[] = [];
  public causes: any;
  public donationRequestForm: FormGroup | undefined;
  constructor(private fb: FormBuilder, private sessionServ: SessionService) {}

  getCompanyInfo() {
    fetch(
      `http://localhost:3001/get-company?companyId=${this.accessTokenPayload.holderId}`
    )
      .then((data) => data.json())
      .then((data) => (this.companyInfo = data));
  }

  getInstitutions() {
    fetch('http://localhost:3001/get-valid-institutions')
      .then((data) => data.json())
      .then((data) => (this.institutions = data));
  }

  async getDonations() {
    await fetch(
      `http://localhost:3001/my-donations?uuid=${this.accessTokenPayload.holderId}`
    )
      .then((data) => data.json())
      .then((data) => (this.donations = data));
  }

  async getPayments() {
    await fetch(
      `http://localhost:3001/received-payments?uuid=${this.accessTokenPayload.holderId}`
    )
      .then((data) => data.json())
      .then((data) => (this.payments = data));
  }

  getPendingUsers() {
    fetch('http://localhost:3001/pending-company-users')
      .then((data) => data.json())
      .then((data) => (this.pendingUsers = data));
  }

  getPendingDonations() {
    fetch('http://localhost:3001/pending-donations')
      .then((data) => data.json())
      .then((data) => (this.pendingDonations = data));
  }
  acceptDonation(form: any) {
    const formValue = form.value;
    fetch('http://localhost:3001/accept-company-donation', {
      method: 'POST',
      body: new URLSearchParams({
        value: formValue.value,
        institutionId: formValue.institutionId,
        causeid: formValue.causeId,
        description: formValue.description,
        pending_donationid: formValue.pending_donation_id,
      }),
    }).then(() => this.getPendingDonations());
  }

  rejectDonation(form: any) {
    fetch('http://localhost:3001/reject-company-donation', {
      method: 'POST',
      body: new URLSearchParams({
        pending_donationid: form.value.pending_donationid,
      }),
    }).then(() => this.getPendingDonations());
  }

  acceptUser(form: any) {
    fetch('http://localhost:3001/accept-user-company', {
      method: 'POST',
      body: new URLSearchParams({
        userId: form.value.userId,
        companyId: form.value.companyId,
      }),
    }).then(() => this.getPendingUsers());
  }

  rejectUser(form: any) {
    fetch('http://localhost:3001/reject-user-company', {
      method: 'POST',
      body: new URLSearchParams({
        userId: form.value.userId,
        companyId: form.value.companyId,
      }),
    }).then(() => this.getPendingUsers());
  }

  getCausesFromInstitution(institutionId: any) {
    if (!institutionId.match('Select institution')) {
      fetch(`http://localhost:3001/get-causes?institutionId=${institutionId}`)
        .then((data) => data.json())
        .then((data) => (this.causes = data));
    } else {
      this.causes = null;
    }
  }

  submitDonationRequest() {
    if (this.donationRequestForm) {
      const formValue = this.donationRequestForm.value;

      fetch('http://localhost:3001/register-company-donation', {
        method: 'POST',
        body: new URLSearchParams({
          institutionId: formValue.institution,
          amount: formValue.amount,
          description: formValue.message,
          causeId: formValue.cause == '' ? null : formValue.cause,
        }),
      });
    }
  }

  async ngOnInit(): Promise<void> {
    if (await this.sessionServ.doesSessionExist()) {
      await this.sessionServ.refreshAccessToken();
      this.accessTokenPayload = this.sessionServ.accessToken;
      this.getCompanyInfo();
      this.getPendingUsers();
      this.getPendingDonations();
      this.getInstitutions();
      this.getDonations().then(() => {
        this.getPayments().then(() => {
          let mergedArray = this.donations.concat(this.payments);
          this.combinedArray = mergedArray.slice().sort((a, b) => {
            return Date.parse(a.date) - Date.parse(b.date);
          });
          this.combinedArray = this.combinedArray.reverse();
        });
      });

      this.donationRequestForm = this.fb.group({
        amount: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
        institution: [
          'Select institution',
          [
            Validators.required,
            Validators.pattern('^(?!.*Select institution).*$'),
          ],
        ],
        cause: [''],
        message: ['', [Validators.maxLength(200)]],
      });
      this.donationRequestForm
        .get('institution')
        ?.valueChanges.subscribe((data) => this.getCausesFromInstitution(data));
    }
  }
  displayedColumnsHistory: string[] = [
    'Name',
    'Institution',
    'Cause',
    'Type',
    'Description',
    'Date',
    'Value',
  ];

  get amount() {
    return this.donationRequestForm?.get('amount');
  }
  get institution() {
    return this.donationRequestForm?.get('institution');
  }
  get cause() {
    return this.donationRequestForm?.get('cause');
  }
  get message() {
    return this.donationRequestForm?.get('message');
  }
  get cardName() {
    return this.donationRequestForm?.get('cardName');
  }
  get cardNumber() {
    return this.donationRequestForm?.get('cardNumber');
  }
  get expireDate() {
    return this.donationRequestForm?.get('expireDate');
  }
  get ccv() {
    return this.donationRequestForm?.get('ccv');
  }
}

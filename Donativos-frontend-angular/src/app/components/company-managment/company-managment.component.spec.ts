import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyManagmentComponent } from './company-managment.component';

describe('CompanyManagmentComponent', () => {
  let component: CompanyManagmentComponent;
  let fixture: ComponentFixture<CompanyManagmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyManagmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyManagmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SessionService } from 'src/app/session.service';

@Component({
  selector: 'app-admin-institution-page',
  templateUrl: './admin-institution-page.component.html',
  styleUrls: ['./admin-institution-page.component.css'],
})
export class AdminInstitutionPageComponent implements OnInit {
  public accessTokenPayload: any | null;
  public causes: any[] = [];
  public pendingCauses: any[] = [];
  public pendingUsers: any[] = [];
  public pendingPayments: any[] = [];
  public validCompanies: any[] = [];
  public donations: any[] = [];
  public payments: any[] = [];
  public combinedArray: any[] = [];
  public causeForm: FormGroup | undefined;
  public paymentRequestForm: FormGroup | undefined;
  public balance: any;
  public institutionInfo: any;
  public validCause: boolean = false;
  constructor(private fb: FormBuilder, private sessionServ: SessionService) {}

  acceptCause(form: any) {
    fetch('http://localhost:3001/accept-cause', {
      method: 'POST',
      body: new URLSearchParams({
        causeId: form.value.id,
      }),
    }).then(() => this.getPendingCauses());
  }

  rejectCause(form: any) {
    fetch('http://localhost:3001/reject-cause', {
      method: 'POST',
      body: new URLSearchParams({
        causeId: form.value.id,
      }),
    }).then(() => this.getPendingCauses());
  }

  acceptUser(form: any) {
    fetch('http://localhost:3001/accept-user-institution', {
      method: 'POST',
      body: new URLSearchParams({
        userId: form.value.userId,
        institutionId: form.value.institutionId,
      }),
    }).then(() => this.getPendingUsers());
  }

  rejectUser(form: any) {
    fetch('http://localhost:3001/reject-user-institution', {
      method: 'POST',
      body: new URLSearchParams({
        userId: form.value.userId,
        institutionId: form.value.institutionId,
      }),
    }).then(() => this.getPendingUsers());
  }

  acceptPayment(form: any) {
    fetch('http://localhost:3001/accept-institution-payment', {
      method: 'POST',
      body: new URLSearchParams({
        pending_paymentid: form.value.id,
      }),
    }).then(() => this.getPendingPayments());
  }

  rejectPayment(form: any) {
    fetch('http://localhost:3001/reject-institution-payment', {
      method: 'POST',
      body: new URLSearchParams({
        pending_paymentid: form.value.id,
      }),
    }).then(() => this.getPendingPayments());
  }

  getBalance() {
    fetch('http://localhost:3001/get-institution-balance')
      .then((data) => data.json())
      .then((data) => (this.balance = data));
  }

  submitCause() {
    if (this.causeForm) {
      let value = this.causeForm.value;
      fetch('http://localhost:3001/post-causes', {
        method: 'POST',
        body: new URLSearchParams({
          name: value.name,
          description: value.description,
          goal: value.goal,
          institutionId: value.institutionId,
        }),
      }).then(() => this.getCauses());
    }
  }

  getInstitutionInfo() {
    fetch(
      `http://localhost:3001/get-institution?institutionId=${this.accessTokenPayload.holderId}`
    )
      .then((data) => data.json())
      .then((data) => (this.institutionInfo = data));
  }

  getValidCompanies() {
    fetch('http://localhost:3001/get-valid-companies')
      .then((data) => data.json())
      .then((data) => (this.validCompanies = data));
  }

  getPendingUsers() {
    fetch('http://localhost:3001/pending-institution-users')
      .then((data) => data.json())
      .then((data) => (this.pendingUsers = data));
  }

  getPendingCauses() {
    fetch('http://localhost:3001/pending-causes')
      .then((data) => data.json())
      .then((data) => (this.pendingCauses = data));
  }

  getPendingPayments() {
    fetch('http://localhost:3001/pending-payments')
      .then((data) => data.json())
      .then((data) => (this.pendingPayments = data));
  }

  getCauses() {
    fetch(
      `http://localhost:3001/get-causes?institutionId=${this.accessTokenPayload.holderId}`
    )
      .then((data) => data.json())
      .then((data) => (this.causes = data));
  }

  submitPaymentRequest() {
    if (this.paymentRequestForm) {
      const formValue = this.paymentRequestForm.value;
      fetch('http://localhost:3001/register-payment', {
        method: 'POST',
        body: new URLSearchParams({
          receiver: formValue.company,
          amount: formValue.amount,
          description: formValue.message,
          cause: this.validCause ? formValue.cause : '',
        }),
      }).then(() => this.getPendingPayments());
    }
  }

  async getDonations() {
    await fetch(
      `http://localhost:3001/received-donations?uuid=${this.accessTokenPayload.holderId}`
    )
      .then((data) => data.json())
      .then((data) => (this.donations = data));
  }

  async getPayments() {
    await fetch(
      `http://localhost:3001/my-payments?uuid=${this.accessTokenPayload.holderId}`
    )
      .then((data) => data.json())
      .then((data) => (this.payments = data));
  }

  async ngOnInit(): Promise<void> {
    await this.sessionServ.refreshAccessToken();
    this.accessTokenPayload = this.sessionServ.accessToken;
    this.getPendingCauses();
    this.getPendingUsers();
    this.getPendingPayments();
    this.getBalance();
    this.getCauses();
    this.getInstitutionInfo();
    this.getValidCompanies();
    this.getDonations().then(() => {
      this.getPayments().then(() => {
        let mergedArray = this.donations.concat(this.payments);
        this.combinedArray = mergedArray.slice().sort((a, b) => {
          return Date.parse(a.date) - Date.parse(b.date);
        });
        this.combinedArray = this.combinedArray.reverse();
      });
    });
    this.paymentRequestForm = this.fb.group({
      amount: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      company: [
        'Select company',
        [Validators.required, Validators.pattern('^(?!.*Select company).*$')],
      ],
      cause: ['General expense'],
      message: ['', [Validators.maxLength(200)]],
    });
    this.paymentRequestForm
      .get('cause')
      ?.valueChanges.subscribe((data) =>
        data == 'General expense'
          ? (this.validCause = false)
          : (this.validCause = true)
      );
    this.causeForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      goal: ['', [Validators.required]],
      description: ['', [Validators.required, Validators.maxLength(500)]],
      institutionId: [this.accessTokenPayload.holderId, [Validators.required]],
    });
  }
  displayedColumnsCauses: string[] = [
    'Name',
    'Description',
    'Goal',
    'CreationDate',
  ];
  displayedColumnsHistory: string[] = [
    'Name',
    'Cause',
    'Type',
    'Description',
    'Date',
    'Value',
  ];
  get amount() {
    return this.paymentRequestForm?.get('amount');
  }
  get cause() {
    return this.paymentRequestForm?.get('cause');
  }
  get company() {
    return this.paymentRequestForm?.get('company');
  }

  get message() {
    return this.paymentRequestForm?.get('message');
  }
}

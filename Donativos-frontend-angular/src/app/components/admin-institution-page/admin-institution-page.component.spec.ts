import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminInstitutionPageComponent } from './admin-institution-page.component';

describe('AdminInstitutionPageComponent', () => {
  let component: AdminInstitutionPageComponent;
  let fixture: ComponentFixture<AdminInstitutionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminInstitutionPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminInstitutionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

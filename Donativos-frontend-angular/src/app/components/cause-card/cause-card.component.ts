import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cause-card',
  templateUrl: './cause-card.component.html',
  styleUrls: ['./cause-card.component.css']
})
export class CauseCardComponent implements OnInit {
  @Input()
  info: any;

  constructor() { }

  ngOnInit(): void {
  }

}

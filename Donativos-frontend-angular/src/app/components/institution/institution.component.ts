import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { ActivatedRoute } from '@angular/router';
import SwiperCore, { Pagination, Navigation } from 'swiper';

SwiperCore.use([Pagination, Navigation]);

@Component({
  selector: 'app-institution',
  templateUrl: './institution.component.html',
  styleUrls: ['./institution.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class InstitutionComponent implements OnInit {
  public id: any;
  public institutionInfo: any;
  public causes: any;
  public donations: any[] = [];
  public payments: any[] = [];
  public combinedArray: any[] = []

  constructor(private route: ActivatedRoute) {}

  getInstitution() {
    fetch(`http://localhost:3001/get-institution?institutionId=${this.id}`)
      .then((data) => data.json())
      .then((data) => this.institutionInfo = data);
  }
  getCauses() {
    fetch(`http://localhost:3001/get-causes?institutionId=${this.id}`)
      .then((data) => data.json())
      .then((data) => (this.causes = data));
  }

  async getDonations() {
    await fetch(
      `http://localhost:3001/received-donations?uuid=${this.id}`
    )
      .then((data) => data.json())
      .then((data) => (this.donations = data));
  }

  async getPayments(){
    await fetch(`http://localhost:3001/my-payments?uuid=${this.id}`)
    .then(data => data.json())
    .then(data => this.payments = data)
  }
  ngOnInit(): void {
    this.route.params.subscribe((data) => {
      this.id = data['id'];
      this.getInstitution();
      this.getCauses();
      this.getDonations()
      .then(()=>{
        this.getPayments()
        .then(() =>{
          let mergedArray = this.donations.concat(this.payments)
          this.combinedArray = mergedArray.slice().sort((a,b) => {return Date.parse(a.date) - Date.parse(b.date)})
          this.combinedArray = this.combinedArray.reverse();
        })
      })
      
    });
  }
  displayedColumns: string[] = ['Name', 'Cause', 'Type', 'Description', 'Date', 'Value'];
}

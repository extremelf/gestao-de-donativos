import { Component, OnInit } from '@angular/core';

import {
  HttpClient,
  HttpEvent,
  HttpParams,
  HttpResponse,
} from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import SuperTokens from 'supertokens-website';
import { SessionService } from 'src/app/session.service';

SuperTokens.init({ apiDomain: 'http://localhost:3001', apiBasePath: '/auth' });
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  public submited: boolean = false;
  public userId: any = 'ola';
  public accessTokenPayload: any | null;
  public user: any = 'Loading';
  public donations: any[] = [];
  public institutions: any[] = [];
  public companies: any[] = [];
  public institutionRequestForm: FormGroup = this.fb.group({
    institutionId: [
      'Open this to select an Institution',
      [Validators.required],
    ],
  });
  public companyRequestForm: FormGroup = this.fb.group({
    companyId: ['Open this to select a Company', [Validators.required]],
  });
  public institutionCreationForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    nif: [
      '',
      [
        Validators.required,
        Validators.pattern(/\d{9}/),
        Validators.maxLength(9),
      ],
    ],
    description: ['', [Validators.required, Validators.maxLength(500)]],
  });
  public companyCreationForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    nif: [
      '',
      [
        Validators.required,
        Validators.pattern(/\d{9}/),
        Validators.maxLength(9),
      ],
    ],
    description: ['', [Validators.required, Validators.maxLength(500)]],
  });
  constructor(private fb: FormBuilder, private sessionServ: SessionService) {}

  async submitInstitution() {
    const formValue = this.institutionCreationForm.value;
    fetch('http://localhost:3001/post-institutions', {
      method: 'POST',
      body: new URLSearchParams({
        name: formValue.name,
        nif: formValue.nif,
        description: formValue.description,
      }),
    });
    this.sessionServ.refreshAccessToken();
    this.accessTokenPayload = this.sessionServ.accessToken;
    this.submited = true;
  }

  async submitCompany() {
    const formValue = this.companyCreationForm.value;
    fetch('http://localhost:3001/post-companies', {
      method: 'POST',
      body: new URLSearchParams({
        name: formValue.name,
        nif: formValue.nif,
        description: formValue.description,
      }),
    });
    this.sessionServ.refreshAccessToken();
    this.accessTokenPayload = this.sessionServ.accessToken;
    this.submited = true;
  }

  getUserInfo() {
    fetch('http://localhost:3001/userInfo')
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        this.user = data;
      });
  }

  getInstitutions() {
    fetch('http://localhost:3001/get-valid-institutions')
      .then((data) => data.json())
      .then((data) => (this.institutions = data));
  }

  getCompanies() {
    fetch('http://localhost:3001/get-valid-companies')
      .then((data) => data.json())
      .then((data) => (this.companies = data));
  }

  submitInstitutionRequest() {
    const formValue = this.institutionRequestForm.value;
    fetch('http://localhost:3001/request-association-institution', {
      method: 'POST',
      body: new URLSearchParams({
        institutionId: formValue.institutionId,
      }),
    });
  }

  submitCompanyRequest() {
    const formValue = this.companyRequestForm.value;
    fetch('http://localhost:3001/request-association-company', {
      method: 'POST',
      body: new URLSearchParams({
        companyId: formValue.companyId,
      }),
    });
  }

  getMyDonations() {
    fetch(
      `http://localhost:3001/my-donations?uuid=${this.accessTokenPayload.email}`
    )
      .then((data) => data.json())
      .then((data) => (this.donations = data));
  }

  async ngOnInit(): Promise<void> {
    if (await SuperTokens.doesSessionExist()) {
      this.userId = await SuperTokens.getUserId();
      await this.sessionServ.refreshAccessToken();
      this.accessTokenPayload = this.sessionServ.accessToken;
      this.institutionIdSelectedInvalid;
      this.getUserInfo();
      this.getInstitutions();
      this.getCompanies();
      this.getMyDonations();
    }
  }

  get companyId() {
    return this.companyRequestForm.get('companyId');
  }

  get companyIdSelectedInvalid() {
    return this.companyId?.value == 'Open this to select a Company';
  }
  get institutionId() {
    return this.institutionRequestForm.get('institutionId');
  }

  get institutionIdSelectedInvalid() {
    return this.institutionId?.value == 'Open this to select an Institution';
  }

  get companyName() {
    return this.companyCreationForm.get('name');
  }
  get companyNif() {
    return this.companyCreationForm.get('nif');
  }
  get companyDescription() {
    return this.companyCreationForm.get('description');
  }
  get institutionName() {
    return this.institutionCreationForm.get('name');
  }
  get institutionNif() {
    return this.institutionCreationForm.get('nif');
  }
  get institutionDescription() {
    return this.institutionCreationForm.get('description');
  }
}

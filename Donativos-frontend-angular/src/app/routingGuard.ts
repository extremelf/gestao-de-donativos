import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Route, Router, RouterStateSnapshot } from '@angular/router';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root',
})
export class RoutingGuard implements CanActivate {
  public accessToken: any;
  constructor(private sessionServ: SessionService, private router: Router, private route: ActivatedRoute) {}

  async canActivate(): Promise<boolean> {
    if (!await this.sessionServ.doesSessionExist()) {     
      this.router.navigate(['']);
    }
    return await this.sessionServ.doesSessionExist();
  }
}

import { NgModule } from '@angular/core';

import {RouterModule, Routes} from "@angular/router";

import { BrowserModule } from '@angular/platform-browser';
import {ProfileComponent} from "./components/profile/profile.component";
import {HomeComponent} from "./components/home/home.component";
import {DonationComponent} from "./components/donation/donation.component";
import {InstitutionComponent} from "./components/institution/institution.component";
import {CompanyManagmentComponent} from "./components/company-managment/company-managment.component";
import {CauseComponent} from "./components/cause/cause.component";
import { RoutingGuard } from './routingGuard';

import {AdminPageComponent} from "./components/admin-page/admin-page.component";
import {AdminInstitutionPageComponent} from "./components/admin-institution-page/admin-institution-page.component";


const routes: Routes = [
  {
    path:'', component:HomeComponent
  },
  {
    path: 'profile',canActivate: [RoutingGuard], component: ProfileComponent
  },
  {
    path: 'home', component:HomeComponent
  },
  {
    path: 'donation', component:DonationComponent
  },
  {
    path:'institution/:id', component:InstitutionComponent
  },
  {
    path:'companyManagement',canActivate: [RoutingGuard], component:CompanyManagmentComponent
  },
  {
    path:'causes/:id', component:CauseComponent
  },
  {
    path:'admin',canActivate: [RoutingGuard], component: AdminPageComponent
  },
  {
    path:'adminInstitutionPage',canActivate: [RoutingGuard], component:AdminInstitutionPageComponent
  }



];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule
  ],
  providers: [ RoutingGuard],
  exports:[RouterModule]
})
export class AppRoutingModule { }

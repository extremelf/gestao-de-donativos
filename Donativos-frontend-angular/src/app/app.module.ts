import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from '@auth0/auth0-angular';

import { ProfileComponent } from './components/profile/profile.component';
import { HomeComponent } from './components/home/home.component';
import { NavComponent } from './components/nav/nav.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FooterComponent } from './components/footer/footer.component';
import { UserHistoryComponent } from './components/user-history/user-history.component';
import { DonationComponent } from './components/donation/donation.component';
import { InstitutionComponent } from './components/institution/institution.component';
import { SwiperModule } from 'swiper/angular';
import { CompanyManagmentComponent } from './components/company-managment/company-managment.component';
import { CauseComponent } from './components/cause/cause.component';
import { CauseCardComponent } from './components/cause-card/cause-card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { AdminInstitutionPageComponent } from './components/admin-institution-page/admin-institution-page.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SessionService } from './session.service';
import { NumbersonlyDirective } from '../directives/numbersonly.directive';

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    HomeComponent,
    NavComponent,
    SidebarComponent,
    FooterComponent,
    UserHistoryComponent,
    DonationComponent,
    InstitutionComponent,
    CompanyManagmentComponent,
    CauseComponent,
    CauseCardComponent,
    AdminPageComponent,
    AdminInstitutionPageComponent,
    NumbersonlyDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SwiperModule,
    AuthModule.forRoot({
      domain: 'dev-h39ih9n9.eu.auth0.com',
      clientId: '7M19rmz8slAzccEOqdni1lThoH5r3cGU',
    }),
    BrowserAnimationsModule,
    MatTableModule,
    MatSortModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
  ],
  providers: [SessionService],
  bootstrap: [AppComponent],
})
export class AppModule {}

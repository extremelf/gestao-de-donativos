import { Component } from '@angular/core';
import SuperTokens from 'supertokens-website';

SuperTokens.init({ apiDomain: 'http://localhost:3001', apiBasePath: '/auth' });

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'donativosfrontendv1';

  test = 1
}

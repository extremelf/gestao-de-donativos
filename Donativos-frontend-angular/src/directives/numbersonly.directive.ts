import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appNumbersonly]',
})
export class NumbersonlyDirective {
  private regex: RegExp = new RegExp('^[0-9]*$');
  constructor(private elementRef: ElementRef) {}

  @HostListener('keydown', ['$event']) onKeyDown(event: KeyboardEvent) {
    const inputValue: String = this.elementRef.nativeElement.value.concat(
      event.key
    );
    if (inputValue && !inputValue.match('Backspace')) {
      if (!String(inputValue).match(this.regex)) {
        event.preventDefault();
      }
      return;
    }
    return;
  }

  @HostListener('paste', ['$event']) onPaste(event: any) {
    const clipboardData = (event.originalEvent || event).clipboardData.getData(
      'text/plain'
    );
    if (clipboardData && !this.regex.test(clipboardData)) {
      event.preventDefault();
    }
    return;
  }
}


import "./App.css";
import SuperTokens, {getSuperTokensRoutesForReactRouterDom} from "supertokens-auth-react";
import EmailPassword from "supertokens-auth-react/recipe/emailpassword";
import Session from "supertokens-auth-react/recipe/session";
import { BrowserRouter as Router, Routes, Route} from "react-router-dom";
SuperTokens.init({
  appInfo: {
    // learn more about this on https://supertokens.com/docs/emailpassword/appinfo
    appName: "Donativos",
    apiDomain: "http://localhost:3001",
    websiteDomain: "http://localhost:4200",
    apiBasePath: "/auth",
    websiteBasePath: "/auth",
  },
  recipeList: [EmailPassword.init({
        signInAndUpFeature:{
          signUpForm:{
            termsOfServiceLink:"",
            privacyPolicyLink:"",
            formFields:[{
              id: "Name",
              label:"Username",
              placeholder:"Please enter your Username"
              },
              {
                id: "Address",
                label:"Address",
                placeholder:"Please enter your Address"
              },
              {
                id: "vat",
                label:"VAT",
                placeholder: "Please enter your VAT",
                validate: async(value) => {
                  if(/\d{9}/.test(value).valueOf()){
                    return undefined
                  }
                  return "VAT must have 9 digits"
                }
              }
              ]
          }
        }

  }), Session.init()],
});

function App() {
  return (
    <div className="App">
      <Router>
        <div className="fill">
          <Routes>
          {getSuperTokensRoutesForReactRouterDom(require("react-router-dom"))}
            <Route path="/">
              
            </Route>
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;

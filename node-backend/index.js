const express = require("express");
const fetch = require("node-fetch");
const dotenv = require("dotenv");
const pgPool = require("./dbConnection");
let supertokens = require("supertokens-node");
let Recipe = require("supertokens-node/recipe/emailpassword");
let Session = require("supertokens-node/recipe/session");
let EmailPassword = require("supertokens-node/recipe/emailpassword");
let cors = require("cors");
let {
  middleware,
  errorHandler,
} = require("supertokens-node/framework/express");
let {
  verifySession,
} = require("supertokens-node/recipe/session/framework/express");
let { SessionRequest } = require("supertokens-node/framework/express");
const {
  getInfoFromAccessToken,
} = require("supertokens-node/lib/build/recipe/session/accessToken");
const {
  default: SuperTokens,
} = require("supertokens-node/lib/build/supertokens");

let PORT = 3001;
let HOST = "localhost";
dotenv.config();
const app = express();


//inicialização do supertokens
supertokens.init({
  framework: "express",
  supertokens: {
    //url do core do supertokens que está a correr em docker
    connectionURI: "http://localhost:3567/",
  },
  appInfo: {
    //informação acerca da aplicação e a path da autenticação
    appName: "Donativos",
    apiDomain: "http://localhost:3000",
    websiteDomain: "http://localhost:4200", //dominio para o qual o supertokens deverá redirecionar após sign in/sign up
    apiBasePath: "/auth",
    websiteBasePath: "/auth",
  },
  recipeList: [
    EmailPassword.init({
      signUpFeature: {
        //campos extra do formulário de sign up
        formFields: [
          {
            id: "Name",
          },
          {
            id: "Address",
          },
          {
            id: "vat",
            validate: async (value) => {
              if (/\d{9}/.test(value).valueOf()) {
                return undefined;
              }
              return "VAT must have 9 digits";
            },
          },
        ],
      },
      override: {
        functions: (originalImplementation) => {
          return {
            ...originalImplementation,
            //override a função default de signup para acomodar o processo de criação de conta na blockchain durante o processo de signup na plataforma web
            signUp: async function (input) {
              let email = input.email;

              let corda_uuid = await fetch(
                "http://localhost:50005/create-account",
                {
                  method: "POST",
                  headers: {
                    "Content-type": "application/x-www-form-urlencoded",
                  },
                  body: new URLSearchParams({
                    email: `${email}`,
                  }),
                }
              )
                .then((response) => response.text())
                .then((response) => (response = JSON.parse(response)));
              corda_uuid = JSON.parse(corda_uuid);
              let originalResponse = await originalImplementation.signUp(input);
              //após o utilizador ser criado na database do supertokens este é inserido na nossa database e é colocada
              // a referência ao id do user na base de dados do supertokens, de forma a poder cruzar dados entre as duas bases de dados
              const client = await pgPool.connect();
              try {
                client.query(
                  "INSERT INTO users(user_id,corda_uuid) values($1,$2)",
                  [originalResponse.user.id, corda_uuid.uuid]
                );
              } finally {
                client.release();
              }

              return originalResponse;
            },
          };
        },
        apis: (originalImplementation) => {
          return {
            ...originalImplementation,
            //função a ser executada após signup para introdução na nossa database dos dados extra do formulário de sign up
            signUpPOST: async function (input) {
              if (originalImplementation.signUpPOST === undefined) {
                throw Error("Should never come here");
              }
              let response = await originalImplementation.signUpPOST(input);
              if (response.status === "OK") {
                let formFields = input.formFields;
                pgPool.query(
                  "UPDATE users SET name = $1, address = $2, nif = $3 where user_id = $4",
                  [
                    formFields[2].value,
                    formFields[3].value,
                    formFields[4].value,
                    response.user.id,
                  ]
                );
              }

              return response;
            },
          };
        },
      },
    }), // initializes signin / sign up features
    Session.init({
      override: {
        functions: (originalImplementation) => {
          return {
            ...originalImplementation,

            //função para costumizar os dados guardados na sessão, incluindo assim dados costumizados de forma a facilitar as operações no frontend e backend sem necessidade de contactar a base
            //de dados em diversos casos. Informação envolve instituição/empresa (campo holderId) a que o user está associado, bem como se este é administrador da mesma, etc
            createNewSession: async function (input) {
              let stUserId = input.userId;
              let corda_uuid;
              let userId = await pgPool.query(
                "SELECT * FROM users WHERE user_id = $1",
                [stUserId]
              );
              let institutionId = null;
              let holderId = null;
              let isAdmin = null;
              let isHolderAdmin = false;
              let hasCompany = false;
              let hasInstitution = false;
              let isHolderPendingJoin = null;
              let userInternalId = null;
              let user = await EmailPassword.getUserById(stUserId);

              const client = await pgPool.connect();
              try {
                let res = await client.query(
                  "SELECT isadmin, corda_uuid, id FROM users WHERE user_id = $1",
                  [stUserId]
                );
                corda_uuid = res.rows[0].corda_uuid
                  ? res.rows[0].corda_uuid
                  : undefined;
                isAdmin = res.rows[0].isadmin ? res.rows[0].isadmin : false;
                userInternalId = res.rows[0].id;
              } finally {
                client.release();
              }

              let institution_from_user = await pgPool.query(
                "SELECT * FROM user_institution WHERE user_id = $1",
                [userId.rows[0].id]
              );

              if (institution_from_user.rowCount > 0) {
                holderId = institution_from_user.rows[0].institution_id;
                isHolderAdmin = institution_from_user.rows[0].isadmin;
                isHolderPendingJoin = institution_from_user.rows[0].ispending;
                hasInstitution = true;
              } else {
                let company_from_user = await pgPool.query(
                  "SELECT * FROM user_company WHERE user_id = $1",
                  [userId.rows[0].id]
                );

                if (company_from_user.rowCount > 0) {
                  holderId = company_from_user.rows[0].company_id;
                  isHolderAdmin = company_from_user.rows[0].isadmin;
                  isHolderPendingJoin = company_from_user.rows[0].ispending;
                  hasCompany = true;
                }
              }
              //dados de sessão que serão acedidos no frontend
              input.accessTokenPayload = {
                ...input.accessTokenPayload,
                corda_uuid: corda_uuid,
                holderId: holderId,
                isHolderAdmin: isHolderAdmin,
                isHolderPendingJoin: isHolderPendingJoin,
                hasCompany: hasCompany,
                hasInstitution: hasInstitution,
                email: user.email,
                isAdmin: isAdmin,
                userInternalId: userInternalId,
              };
              // dados de sessão armazenados na base de dados do supertokens
              input.sessionData = {
                ...input.sessionData,
                corda_uuid: corda_uuid,
                holderId: holderId,
                isHolderAdmin: isHolderAdmin,
                isHolderPendingJoin: isHolderPendingJoin,
                hasCompany: hasCompany,
                hasInstitution: hasInstitution,
                user: user.email,
                isAdmin: isAdmin,
                userInternalId: userInternalId,
              };

              return originalImplementation.createNewSession(input);
            },
          };
        },
      },
    }), // initializes session features
  ],
});

app.use(
  cors({
    origin: "http://localhost:3000",
    allowedHeaders: ["content-type", ...supertokens.getAllCORSHeaders()],
    credentials: true,
  })
);

app.use(middleware());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//criação do utilizar para as transações anónimas e do utilizador administrador caso este ainda não exista
const defaultUsersInit = async () => {
  let anonUser = await Recipe.getUserByEmail("anonimo@anonimo.com");
  anonUser
    ? console.log("Anon user already created")
    : await Recipe.signUp("anonimo@anonimo.com", "beryGudPassword");
  let adminUser = await Recipe.getUserByEmail("admin@admin.com");
  adminUser
    ? console.log("Admin user already created")
    : await Recipe.signUp("admin@admin.com", "beryGudPassword");
};

const startServer = async () => {
  await defaultUsersInit();

  app.get("/get-user-info", verifySession(), async (req, res) => {
    let userId = req.session.userId; // You can learn more about the `User` object over here https://github.com/supertokens/core-driver-interface/wiki
    let userInfo = await EmailPassword.getUserById(userId); // ...
    res.send(userInfo);
  });

  //obter informação acerca de uma causa expecífica
  app.get("/get-cause", async (req, res) => {
    pgPool.query(
      "SELECT * FROM causes WHERE id = $1",
      [req.query.causeId],
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          pgPool.query(
            "SELECT * FROM institution WHERE id = $1",
            [result.rows[0].institutionid],
            (error, result2) => {
              res.send({
                ...result.rows[0],
                institutionName: result2.rows[0].name,
              });
            }
          );
        }
      }
    );
  });
  //obter informação acerca de todas as causas validadas pelos administradores de instituições
  app.get("/get-causes", async (req, res) => {
    pgPool.query(
      "SELECT * FROM causes WHERE institutionid = $1 and isaccepted = true",
      [req.query.institutionId],
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          res.send(result.rows);
        }
      }
    );
  });
  //obter informação sobre todas as causas 
  app.get("/get-all-causes", async (req, res) => {
    pgPool.query(
      "SELECT * FROM causes",

      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          res.send(result.rows);
        }
      }
    );
  });
  //obter informação de todos os utilizadores que tenham validação pendente para se juntarem a uma instituição
  app.get("/pending-institution-users", verifySession(), (req, res) => {
    let usersList = [];
    let sessionData = req.session.getAccessTokenPayload();
    if (!sessionData.hasInstitution) {
      res.send(400).json("'error': 'User does not belong to institution'");
    }
    pgPool.query(
      "SELECT * FROM user_institution WHERE ispending = $1 and institution_id = $2",
      [true, sessionData.holderId],
      async (error, result) => {
        if (!result && result.rowCount == 0) {
          res.sendStatus(500);
        } else {
          for (let user of result.rows) {
            let username = await pgPool.query(
              "SELECT name FROM users WHERE id = $1",
              [user.user_id]
            );

            let object = {
              name: username.rows[0].name,
              userId: user.user_id,
              institutionId: user.institution_id,
            };
            usersList.push(object);
          }
          res.send(usersList);
        }
      }
    );
  });
  //aceitação da associação de um utilizador a uma instituição
  app.post("/accept-user-institution", verifySession(), async (req, res) => {
    let sessionData = req.session.getAccessTokenPayload();
    if (!(req.body.institutionid != sessionData.holderId)) {
      res.sendStatus(500);
    } else {
      let userInfo = await pgPool.query(
        "SELECT * FROM users WHERE user_id = $1",
        [req.session.userId]
      );

      if (userInfo.rowCount > 0) {
        pgPool.query(
          "SELECT * FROM user_institution WHERE user_id = $1 and institution_id = $2",
          [userInfo.rows[0].id, sessionData.holderId],
          async (error, result) => {
            if (!result.rows[0].isadmin) {
              res.sendStatus(500);
            } else {

              //atualização do estado do utilizador em relação à instituição
              await pgPool.query(
                "UPDATE user_institution SET ispending = false WHERE user_id = $1 and institution_id = $2",
                [req.body.userId, req.body.institutionId],
                (error, result) => {
                  if (!result) {
                    res.sendStatus(500);
                  }
                  res.sendStatus(200);
                }
              );
            }
          }
        );
      } else {
        res.sendStatus(500);
      }
    }
  });
  //rejeição do pedido de associação de um utilizador, removendo da base de dados o pedido
  app.post("/reject-user-institution", verifySession(), async (req, res) => {
    let sessionData = req.session.getAccessTokenPayload();

    let user = await pgPool.query("SELECT * FROM users WHERE user_id = $1", [
      req.session.userId,
    ]);

    if (user.rowCount > 0) {
      pgPool.query(
        "SELECT * FROM user_institution WHERE user_id = $1 and institution_id = $2",
        [user.rows[0].id, sessionData.holderId],
        async (error, result) => {
          if (!result.rows[0].isadmin) {
            res.sendStatus(500);
          } else {
            await pgPool.query(
              "DELETE FROM user_institution WHERE user_id = $1 and institution_id = $2",
              [req.body.userId, req.body.institutionId]
            );

            res.send(200);
          }
        }
      );
    } else {
      res.sendStatus(500);
    }
  });
  //obter informação acerca das causas que necessitem de validação para uma instituição
  app.get("/pending-causes", verifySession(), (req, res) => {
    let causesList = [];
    //obtenção dos dados de sessão de quem fez o pedido de forma a obter a instituição a que pertence, sem esta ter de ser enviada no body do pedido
    let sessionData = req.session.getAccessTokenPayload();
    if (!sessionData.hasInstitution) {
      res.send(400).json("'error': 'User does not belong to institution'");
    }
    pgPool.query(
      "SELECT * FROM causes WHERE isAccepted = $1 and institutionid = $2",
      [false, sessionData.holderId],
      async (error, result) => {
        if (!result && result.rowCount == 0) {
          res.sendStatus(500);
        } else {
          for (let cause of result.rows) {
            let user = await pgPool.query(
              "SELECT name FROM users WHERE id = $1",
              [cause.creator]
            );
            //tratamento dos dados para facilitar a leitura no frontend
            let object = {
              causeId: cause.id,
              name: cause.name,
              description: cause.description,
              goal: cause.goal,
              institutionId: cause.institutionid,
              createId: cause.creator,
              creator: user.rows[0].name,
            };
            causesList.push(object);
          }
          res.send(causesList);
        }
      }
    );
  });
  //obter informação sobre as doações de uma empresa que estejam pendentes de aprovação
  app.get("/pending-donations", verifySession(), (req, res) => {
    let donationsList = [];
    let sessionData = req.session.getAccessTokenPayload();
    if (!sessionData.hasCompany) {
      res.send(400).json("'error': 'User does not belong to company'");
    } else {
      pgPool.query(
        "SELECT * FROM pending_donations WHERE companyId = $1",
        [sessionData.holderId],
        async (error, result) => {
          if (!result && result.rowCount == 0) {
            res.sendStatus(500);
          } else {
            for (let donation of result.rows) {

              //select dos dados correspondente aos ids para facilitar a leitura ao utilizador
              let user = await pgPool.query(
                "SELECT name FROM users WHERE id = $1",
                [donation.userid]
              );
              let receiver = await pgPool.query(
                "SELECT name FROM institution WHERE id = $1",
                [donation.receiver]
              );
              let cause;
              if (donation.causeid) {
                cause = await pgPool.query(
                  "SELECT * FROM causes WHERE id = $1",
                  [donation.causeid]
                );
              }

              if (user.rowCount == 0 && receiver.rowCount == 0) {
                res.sendStatus(500);
              } else {
                let donationData = {
                  pending_donationid: donation.id,
                  creatorName: user.rows[0].name,
                  creatorId: donation.userid,
                  receiverName: receiver.rows[0].name,
                  receiverId: donation.receiver,
                  causeName: cause ? cause.rows[0].name : "none",
                  causeId: donation.causeid,
                  amount: donation.amount,
                  description: donation.description,
                  companyId: donation.companyid,
                };
                donationsList.push(donationData);
              }
            }
            res.send(donationsList);
          }
        }
      );
    }
  });
  //aceitar uma causa pendente numa instituição
  app.post("/accept-cause", verifySession(), async (req, res) => {
    let sessionData = req.session.getAccessTokenPayload();
    let user = await pgPool.query("SELECT * FROM users WHERE user_id = $1", [
      req.session.userId,
    ]);
    if (user.rowCount == 0) {
      res.sendStatus(500);
    } else {
      pgPool.query(
        "SELECT * FROM causes WHERE id = $1",
        [req.body.causeId],
        (error, result) => {
          if (!result) {
            console.log(error);
            res.sendStatus(500);
          }
          //validação de que esta causa pertence a uma causa associada à instituição do utilizador
          if (result.rows[0].institutionid != sessionData.holderId) {
            res.sendStatus(500);
          }
          pgPool.query(
            "SELECT * FROM user_institution WHERE user_id = $1 and institution_id = $2",
            [user.rows[0].id, sessionData.holderId],
            async (error, result) => {
              //validação de que o utilizador que pretende fazer a aceitação é administrador da instituição
              if (!result.rows[0].isadmin) {
                res.sendStatus(500);
              } else {
                await pgPool.query(
                  "UPDATE causes SET isaccepted = true WHERE id = $1",
                  [req.body.causeId]
                );

                res.sendStatus(200);
              }
            }
          );
        }
      );
    }
  });
  //rejeição de uma causa e consequente remoção da base de dados
  app.post("/reject-cause", verifySession(), async (req, res) => {
    let sessionData = req.session.getAccessTokenPayload();
    pgPool.query(
      "SELECT * FROM causes WHERE id = $1",
      [req.body.causeId],
      async (error, result) => {
        if (
          result.rows[0].institutionid != sessionData.holderId &&
          !sessionData.isHolderAdmin
        ) {
          res.sendStatus(500);
        } else {
          await pgPool.query("DELETE FROM causes WHERE id = $1", [
            req.body.causeId,
          ]);
          res.send(200);
        }
      }
    );
  });

  //introdução na base de dados de uma nova causa, caso o utilizador seja administrador da instituição, ou caso não seja administrador esta será marcada como pendente
  app.post("/post-causes", verifySession(), async (req, res) => {
    if (!req.body.name) {
      res.status(400).json("'error': 'Name not provided'");
    }
    if (!req.body.description) {
      res.status(400).json("'error': 'Description not provided'");
    }
    if (!req.body.goal) {
      res.status(400).json("'error': 'goal not provided'");
    }
    if (!req.body.institutionId) {
      res.status(400).json("'error': 'Institution id not provided'");
    }
    let sessionData = req.session.getAccessTokenPayload();
    if (sessionData.holderId != req.body.institutionId) {
      res.status(400).json("'error': 'User does not belong to institution'");
    }
    pgPool.query(
      "SELECT * FROM users WHERE user_id = $1",
      [req.session.userId],
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          pgPool.query(
            "INSERT INTO causes(name, description, goal, institutionId, isaccepted, creator) VALUES($1, $2, $3, $4, $5, $6)",
            [
              req.body.name,
              req.body.description,
              req.body.goal,
              req.body.institutionId,
              sessionData.isHolderAdmin ? true : false,
              result.rows[0].id,
            ],
            (error, result) => {
              if (!result) {
                res.sendStatus(500);
              } else {
                res.sendStatus(201);
              }
            }
          );
        }
      }
    );
  });

  //obter informação sobre todas as instituições
  app.get("/get-institutions", async (req, res) => {
    pgPool.query("SELECT * FROM institution", (error, result) => {
      if (!result) {
        res.sendStatus(500);
      } else {
        res.send(result.rows);
      }
    });
  });

  //obter informação acerca de todas as instituições aceites na plataforma
  app.get("/get-valid-institutions", async (req, res) => {
    pgPool.query(
      "SELECT * FROM institution WHERE isaccepted = true",
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          res.send(result.rows);
        }
      }
    );
  });
  //obter informação sobre uma instituição específica
  app.get("/get-institution", async (req, res) => {
    pgPool.query(
      "SELECT * FROM institution WHERE id = $1",
      [req.query.institutionId],
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          res.send(result.rows[0]);
        }
      }
    );
  });

  //introdução na base de dados de uma instituição e associação desta com o utilizador criador do pedido, marcando esta como pendente de aprovação de administrador
  app.post("/post-institutions", verifySession(), async (req, res) => {
    if (!req.body.name) {
      res.status(400).json("'error': 'Name not provided'");
    }
    if (!req.body.description) {
      res.status(400).json("'error': 'Description not provided'");
    }
    if (!req.body.nif) {
      res.status(400).json("'error': 'NIF not provided'");
    }

    let inserted_institutionId;

    pgPool.query(
      "INSERT INTO institution(name, description, nif, isaccepted) VALUES($1, $2, $3, $4) RETURNING *",
      [req.body.name, req.body.description, req.body.nif, false],
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          inserted_institutionId = result.rows[0].id;

          let superTokensUserId = req.session.userId;

          let userId;
          pgPool.query(
            "SELECT id FROM users WHERE user_id = $1",
            [superTokensUserId],
            async (error, result) => {
              if (!result) {
                res.sendStatus(500);
              } else {
                userId = result.rows[0].id;

                //atualização dos dados de sessão para acomodar a nova informação sobre o utilizador sem necessitar novo login para atualizar
                let currentTokenPayload = req.session.getAccessTokenPayload();

                currentTokenPayload.hasInstitution = true;
                currentTokenPayload.isHolderAdmin = true;

                await req.session.updateAccessTokenPayload({
                  holderId: inserted_institutionId,
                  ...currentTokenPayload,
                });
                await req.session.updateSessionData({
                  holderId: inserted_institutionId,
                  ...currentTokenPayload,
                });
                pgPool.query(
                  "INSERT INTO user_institution(user_id, institution_id, isadmin, ispending) VALUES($1, $2, $3, $4)",
                  [userId, inserted_institutionId, true, false],
                  (error, result) => {
                    if (!result) {
                      res.sendStatus(500);
                    } else {
                      res.sendStatus(201);
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  });
  //pedido de associação a uma instituição
  app.post(
    "/request-association-institution",
    verifySession(),
    async (req, res) => {
      let user = await pgPool.query("SELECT id FROM users WHERE user_id = $1", [
        req.session.userId,
      ]);

      if (user.rowCount > 0) {
        pgPool.query(
          "INSERT INTO user_institution(user_id, institution_id) VALUES($1, $2)",
          [user.rows[0].id, req.body.institutionId],
          async (error, result) => {
            if (!result) {
              res.sendStatus(500);
            } else {
              //atualização dos dados de sessão para acomodarem os novos dados sem necessitar de novo login
              let currentTokenPayload = req.session.getAccessTokenPayload();

              currentTokenPayload.hasInstitution = true;
              currentTokenPayload.isHolderAdmin = false;

              await req.session.updateAccessTokenPayload({
                holderId: req.body.institutionId,
                ...currentTokenPayload,
              });
              await req.session.updateSessionData({
                holderId: req.body.institutionId,
                ...currentTokenPayload,
              });
              res.sendStatus(201);
            }
          }
        );
      } else {
        res.sendStatus(500);
      }
    }
  );
  //aceitação de uma nova instituição por parte do administrador da plataforma
  app.put("/accept-institution", verifySession(), async (req, res) => {
    if (!req.body.institutionId) {
      res.status(400).json("'error': 'InstitutionID not provided'");
    }
    pgPool.query(
      "SELECT * FROM institution WHERE id = $1",
      [req.body.institutionId],
      async (error, result) => {
        if (!result) {
          res.status(406).send("Institution does not exist");
        } else {
          if (result.rows[0].isaccepted == true) {
            res.status(406).send("Institution already accepted");
          } else {

            //após verificar se a instituição é válida será criada no corda a conta associada a esta instituição
            fetch("http://localhost:50005/create-account", {
              method: "POST",
              headers: {
                "Content-type": "application/x-www-form-urlencoded",
              },
              body: new URLSearchParams({
                email: `${req.body.institutionId}`,
              }),
            })
              .then((response) => response.text())
              .then((response) => JSON.parse(JSON.parse(response)))
              .then((result) => {
                //atualização da informação da instituição com o id no corda
                pgPool.query(
                  "UPDATE institution SET isaccepted = $1, corda_uuid = $2 WHERE id = $3",
                  [true, result.uuid, req.body.institutionId],
                  (error, result) => {
                    if (!result) {
                      res.sendStatus(500);
                    } else {
                      res.sendStatus(200);
                    }
                  }
                );
              });
          }
        }
      }
    );
  });
  //rejeição de instituição apagando os dados da base de dados
  app.delete("/reject-institution", verifySession(), async (req, res) => {
    pgPool.query(
      "DELETE FROM user_institution WHERE user_id = $1 and institution_id = $2",
      [req.body.user_id, req.body.institution_id],
      async (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          pgPool.query(
            "DELETE FROM institution WHERE id = $1",
            [req.body.institution_id],
            (error, result) => {
              res.sendStatus(200);
            }
          );
        }
      }
    );
  });
  //aceitação de empresa, processo semelhante ao processo de aceitação de instituição
  app.put("/accept-company", async (req, res) => {
    if (!req.body.companyId) {
      res.status(400).json("'error': 'CompanyId not provided'");
    }
    pgPool.query(
      "SELECT * FROM company WHERE id = $1",
      [req.body.companyId],
      async (error, result) => {
        if (!result) {
          res.status(406).send("Company does not exist");
        } else {
          if (result.rows[0].isaccepted == true) {
            res.status(406).send("Company already accepted");
          } else {
            fetch("http://localhost:50005/create-account", {
              method: "POST",
              headers: {
                "Content-type": "application/x-www-form-urlencoded",
              },
              body: new URLSearchParams({
                email: `${req.body.companyId}`,
              }),
            })
              .then((response) => response.text())
              .then((response) => JSON.parse(JSON.parse(response)))
              .then((result) => {
                pgPool.query(
                  "UPDATE company SET isaccepted = $1, corda_uuid = $2 WHERE id = $3",
                  [true, result.uuid, req.body.companyId],
                  (error, result) => {
                    if (!result) {
                      res.sendStatus(500);
                    } else {
                      res.sendStatus(200);
                    }
                  }
                );
              });
          }
        }
      }
    );
  });
  //rejeição de empresa, semelhante ao processo de rejeição de instituição
  app.delete("/reject-company", verifySession(), async (req, res) => {
    pgPool.query(
      "DELETE FROM user_company WHERE user_id = $1 and company_id = $2",
      [req.body.user_id, req.body.company_id],
      async (error, result) => {
        if (!result) {
          console.log(error);
          res.sendStatus(500);
        } else {
          pgPool.query(
            "DELETE FROM company WHERE id = $1",
            [req.body.company_id],
            (error, result) => {
              res.sendStatus(200);
            }
          );
        }
      }
    );
  });
  //introdução de um novo pedido de criação de uma empresa, semelhante ao processo para um instituição
  app.post("/post-companies", verifySession(), async (req, res) => {
    if (!req.body.name) {
      res.status(400).json("'error': 'Name not provided'");
    }
    if (!req.body.description) {
      res.status(400).json("'error': 'Description not provided'");
    }
    if (!req.body.nif) {
      res.status(400).json("'error': 'NIF not provided'");
    }

    let inserted_companyId;

    pgPool.query(
      "INSERT INTO company(name, description, nif, isaccepted) VALUES($1, $2, $3, $4) RETURNING *",
      [req.body.name, req.body.description, req.body.nif, false],
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          inserted_companyId = result.rows[0].id;

          let superTokensUserId = req.session.userId;

          let userId;
          pgPool.query(
            "SELECT id FROM users WHERE user_id = $1",
            [superTokensUserId],
            async (error, result) => {
              if (!result) {
                res.sendStatus(500);
              } else {
                userId = result.rows[0].id;
                let currentTokenPayload = req.session.getAccessTokenPayload();

                currentTokenPayload.hasCompany = true;
                currentTokenPayload.isHolderAdmin = true;

                await req.session.updateAccessTokenPayload({
                  holderId: inserted_companyId,
                  ...currentTokenPayload,
                });
                await req.session.updateSessionData({
                  holderId: inserted_companyId,
                  ...currentTokenPayload,
                });
                pgPool.query(
                  "INSERT INTO user_company(user_id, company_id, isadmin, ispending) VALUES($1, $2, $3, $4)",
                  [userId, inserted_companyId, true, false],
                  (error, result) => {
                    if (!result) {
                      console.log(error);
                      res.sendStatus(500);
                    } else {
                      res.sendStatus(201);
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  });

  //obter informação sobre um empresa
  app.get("/get-company", verifySession(), async (req, res) => {
    pgPool.query(
      "SELECT * FROM company WHERE id = $1",
      [req.query.companyId],
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          res.send(result.rows[0]);
        }
      }
    );
  });
  //obter informação sobre todas as empresas
  app.get("/get-companies", async (req, res) => {
    pgPool.query("SELECT * FROM company", (error, result) => {
      if (!result) {
        res.sendStatus(500);
      } else {
        res.send(result.rows);
      }
    });
  });

  //obter informação para as empresas validadas
  app.get("/get-valid-companies", async (req, res) => {
    pgPool.query(
      "SELECT * FROM company WHERE isaccepted = true",
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          res.send(result.rows);
        }
      }
    );
  });

  //obter informação sobre empresas pendentes de aprovação
  app.get("/pending-companies", verifySession(), async (req, res) => {
    pgPool.query(
      "SELECT * FROM company WHERE isaccepted = $1",
      [false],
      async (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          let companyList = [];
          for (company of result.rows) {
            let user = await pgPool.query(
              "SELECT user_id FROM user_company WHERE company_id = $1 AND isadmin = $2",
              [result.rows[0].id, true]
            );
            if (user.rowCount == 0) {
              res.sendStatus(500);
            } else {
              let companyInfo = {
                ...company,
                user_id: user.rows[0].user_id,
              };
              companyList.push(companyInfo);
            }
            res.send(companyList);
          }
        }
      }
    );
  });
  //pedido de associação a empresa, semelhante ao processo de associação a uma instituição
  app.post(
    "/request-association-company",
    verifySession(),
    async (req, res) => {
      let user = await pgPool.query("SELECT id FROM users WHERE user_id = $1", [
        req.session.userId,
      ]);

      if (user.rowCount > 0) {
        pgPool.query(
          "INSERT INTO user_company(user_id, company_id) VALUES($1, $2)",
          [user.rows[0].id, req.body.companyId],
          async (error, result) => {
            if (!result) {
              res.sendStatus(500);
            } else {
              let currentTokenPayload = req.session.getAccessTokenPayload();

              currentTokenPayload.hasCompany = true;
              currentTokenPayload.isHolderAdmin = false;

              await req.session.updateAccessTokenPayload({
                holderId: req.body.companyId,
                ...currentTokenPayload,
              });
              await req.session.updateSessionData({
                holderId: req.body.companyId,
                ...currentTokenPayload,
              });
              res.sendStatus(201);
            }
          }
        );
      } else {
        res.sendStatus(500);
      }
    }
  );

  //utilizadores pendentes de aprovação para se juntarem a uma empresa
  app.get("/pending-company-users", verifySession(), (req, res) => {
    let usersList = [];
    let sessionData = req.session.getAccessTokenPayload();
    if (!sessionData.hasCompany) {
      res.send(400).json("'error': 'User does not belong to Company'");
    }
    pgPool.query(
      "SELECT * FROM user_company WHERE ispending = $1 and company_id = $2",
      [true, sessionData.holderId],
      async (error, result) => {
        if (!result && result.rowCount == 0) {
          res.sendStatus(500);
        } else {
          for (let user of result.rows) {
            let username = await pgPool.query(
              "SELECT name FROM users WHERE id = $1",
              [user.user_id]
            );

            let object = {
              name: username.rows[0].name,
              userId: user.user_id,
              companyId: user.company_id,
            };
            usersList.push(object);
          }
          res.send(usersList);
        }
      }
    );
  });
  //aceitação de um novo utilizador na empresa
  app.post("/accept-user-company", verifySession(), async (req, res) => {
    let sessionData = req.session.getAccessTokenPayload();
    if (!(req.body.institutionid != sessionData.holderId)) {
      res.sendStatus(500);
    } else {
      let userInfo = await pgPool.query(
        "SELECT * FROM users WHERE user_id = $1",
        [req.session.userId]
      );

      if (userInfo.rowCount > 0) {
        pgPool.query(
          "SELECT * FROM user_company WHERE user_id = $1 and company_id = $2",
          [userInfo.rows[0].id, sessionData.holderId],
          async (error, result) => {
            if (!result.rows[0].isadmin) {
              res.sendStatus(500);
            } else {
              await pgPool.query(
                "UPDATE user_company SET ispending = false WHERE user_id = $1 and company_id = $2",
                [req.body.userId, req.body.companyId],
                (error, result) => {
                  if (!result) {
                    res.sendStatus(500);
                  }
                  res.sendStatus(200);
                }
              );
            }
          }
        );
      } else {
        res.sendStatus(500);
      }
    }
  });
  //rejeição de um novo utilizador na empresa
  app.post("/reject-user-company", verifySession(), async (req, res) => {
    let sessionData = req.session.getAccessTokenPayload();

    let user = await pgPool.query("SELECT * FROM users WHERE user_id = $1", [
      req.session.userId,
    ]);

    if (user.rowCount > 0) {
      pgPool.query(
        "SELECT * FROM user_company WHERE user_id = $1 and company_id = $2",
        [user.rows[0].id, sessionData.holderId],
        async (error, result) => {
          if (!result.rows[0].isadmin) {
            res.sendStatus(500);
          } else {
            await pgPool.query(
              "DELETE FROM user_company WHERE user_id = $1 and company_id = $2",
              [req.body.userId, req.body.companyId]
            );

            res.send(200);
          }
        }
      );
    } else {
      res.sendStatus(500);
    }
  });
  //registar uma nova doação por parte de uma empresa, caso o criador do pedido seja o administrador da instituição será feito diretamente sem necessitar de aprovação, caso contrário ficará numa
  //tabela pendente de aprovação pelo administrador
  app.post("/register-company-donation", verifySession(), async (req, res) => {
    // if (!req.body.issuer) {
    //   res.status(400).json("'error': 'Issuer not provided'");
    // }
    let sessionData = req.session.getAccessTokenPayload();
    if (!req.body.institutionId) {
      res.status(400).json("'error': 'Receiver not provided'");
    } else if (!req.body.amount) {
      res.status(400).json("'error': 'Amount not provided'");
    } else {
      let companyCordaId = await pgPool.query(
        "SELECT corda_uuid FROM company WHERE id = $1",
        [sessionData.holderId]
      );
      let company = await pgPool.query("SELECT * FROM company WHERE id = $1", [
        req.body.companyId,
      ]);
      let superTokensUserId = req.session.userId;

      let userId;
      pgPool.query(
        "SELECT id FROM users WHERE user_id = $1",
        [superTokensUserId],
        (error, user) => {
          if (!user && user.rowCount == 0) {
            res.sendStatus(500);
          } else {
            pgPool.query(
              "SELECT * from user_company WHERE user_id = $1 and company_id = $2",
              [user.rows[0].id, sessionData.holderId],
              async (error, user_company) => {
                if (!user_company) {
                  console.log(error);
                  res.sendStatus(500);
                } else {
                  //caso o utilizador seja o administrador o pedido será imediatamente feito para a blockchain
                  if (user_company.rows[0].isadmin) {
                    fetch("http://localhost:50005/create-donation", {
                      method: "POST",
                      headers: {
                        "Content-type": "application/x-www-form-urlencoded",
                      },
                      body: new URLSearchParams({
                        value: req.body.amount,
                        issuer: sessionData.holderId,
                        institution: req.body.institutionId,
                        cause: req.body.causeId,
                        receiver: req.body.institutionId,
                        description: req.body.description,
                      }),
                    })
                      .then((response) => {
                        return response.text();
                      })
                      .then((response) => {
                        res.sendStatus(200);
                      });
                  } else { //caso contrário este será feito para a base de dados
                    let params = [
                      user_company.rows[0].user_id,
                      req.body.institutionId,
                      req.body.amount,
                      req.body.description,
                      sessionData.holderId,
                    ];
                    if (req.body.causeid) {
                      params = [...params, req.body.causeid];
                    }
                    pgPool.query(
                      `INSERT INTO pending_donations(userid, receiver, amount, description, companyid ${
                        req.body.cause ? ", causeid" : ""
                      }) VALUES($1, $2, $3, $4, $5 ${
                        req.body.cause ? ",$6" : ""
                      })`,
                      params,
                      (error, result) => {
                        if (!result) {
                          console.log(error);
                          res.sendStatus(500);
                        } else {
                          res.sendStatus(200);
                        }
                      }
                    );
                  }
                }
              }
            );
          }
        }
      );
    }
  });

  //aceitação do pedido de doação e posterior remoção do pedido da base de dados assim que estiver na blockchain

  app.post("/accept-company-donation", verifySession(), (req, res) => {
    let sessionData = req.session.getAccessTokenPayload();
    fetch("http://localhost:50005/create-donation", {
      method: "POST",
      body: new URLSearchParams({
        value: req.body.value,
        issuer: sessionData.holderId,
        receiver: req.body.institutionId,
        institution: req.body.institutionId,
        cause: req.body.causeid,
        description: req.body.description,
      }),
    })
      .then(() => {
        pgPool.query(
          "DELETE FROM pending_donations WHERE id = $1",
          [req.body.pending_donationid],
          async (error, result) => {
            if (!result) {
              res.sendStatus(500);
            } else {
              res.sendStatus(200);
            }
          }
        );
      })
      .catch(() => res.sendStatus(500));
  });
  //rejeição do pedido de doação e remoção do pedido da base de dados
  app.post("/reject-company-donation", verifySession(), (req, res) => {
    if (!req.body.pending_donationid) {
      res.send(400).json("'error': 'No pending payment id provided'");
    } else {
      pgPool.query(
        "DELETE FROM pending_donations WHERE id = $1",
        [req.body.pending_donationid],
        (error, result) => {
          if (!result) {
            console.log(error);
            res.sendStatus(500);
          } else {
            res.sendStatus(200);
          }
        }
      );
    }
  });

  //utilizadores de uma instituição
  app.get("/users-from-institution", async (req, res) => {
    pgPool.query(
      "SELECT user_id, isAdmin FROM user_institution WHERE institution_id = $1",
      [req.query.institutionId],
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          res.send(result.rows);
        }
      }
    );
  });

  //utilizadores de uma empresa
  app.get("/users-from-company", verifySession(), async (req, res) => {
    pgPool.query(
      "SELECT user_id FROM user-company WHERE company_id = $1",
      [req.query.companyId],
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          res.send(result.rows);
        }
      }
    );
  });


  //obter doações feitas pelo utilizador solicitador deste pedido
  app.get("/my-donations", verifySession(), async (req, res) => {
    let donationList = [];
    const endpoint = new URL("http://localhost:50005/my-donations");
    const params = {
      uuid: req.query.uuid,
    };

    endpoint.search = new URLSearchParams(params);
    fetch(endpoint)
      .then((data) => data.json())
      .then(async (data) => {
        if (Array.isArray(data)) {
          for (let donation of data) {
            let cause;
            donation.cause == ""
              ? (cause = "")
              : (cause = await pgPool.query(
                  "SELECT * FROM causes WHERE id = $1",
                  [donation.cause]
                ));
            let institution = await pgPool.query(
              "SELECT name from institution WHERE id = $1",
              [donation.institution]
            );
            let issuer;
            issuer = await pgPool.query(
              "SELECT id, name FROM users WHERE corda_uuid = $1",
              [donation.issuer]
            );
            if (issuer.rowCount == 0) {
              issuer = await pgPool.query(
                "SELECT id, name FROM company WHERE corda_uuid = $1",
                [donation.issuer]
              );
            }
            let dateArray = donation.date.split(" ");

            let date = new Date(
              Date.parse(
                dateArray[3] +
                  " " +
                  dateArray[1] +
                  " " +
                  dateArray[2] +
                  " " +
                  dateArray[5]
              )
            );

            if (institution.rowCount > 0 && issuer.rowCount > 0) {
              let object = {
                issuerId: issuer.rows[0].id,
                issuer: issuer.rows[0].name,
                causeId: cause.rowCount > 0 ? cause.rows[0].id : null,
                cause: cause.rowCount > 0 ? cause.rows[0].name : cause,
                receiver: institution.rows[0].name,
                receiverId: donation.institution,
                description: donation.description,
                date: date,
                value: donation.value,
                type: "Donation",
              };
              donationList.push(object);
            }
          }
        }

        res.send(donationList);
      });
  });
  //obter doações recebidas pelo utilizador do pedido
  app.get("/received-donations", async (req, res) => {
    let donationList = [];
    const endpoint = new URL("http://localhost:50005/received-donations");
    const params = {
      uuid: req.query.uuid,
    };

    endpoint.search = new URLSearchParams(params);
    fetch(endpoint)
      .then((data) => data.json())
      .then(async (data) => {
        if (Array.isArray(data)) {
          for (let donation of data) {
            let cause;
            donation.cause == ""
              ? (cause = "")
              : (cause = await pgPool.query(
                  "SELECT * FROM causes WHERE id = $1",
                  [donation.cause]
                ));
            let institution = await pgPool.query(
              "SELECT name from institution WHERE id = $1",
              [donation.institution]
            );
            let issuer;
            issuer = await pgPool.query(
              "SELECT id, name FROM users WHERE corda_uuid = $1",
              [donation.issuer]
            );
            if (issuer.rowCount == 0) {
              issuer = await pgPool.query(
                "SELECT id, name FROM company WHERE corda_uuid = $1",
                [donation.issuer]
              );
            }
            let dateArray = donation.date.split(" ");

            let date = new Date(
              Date.parse(
                dateArray[3] +
                  " " +
                  dateArray[1] +
                  " " +
                  dateArray[2] +
                  " " +
                  dateArray[5]
              )
            );

            if (institution.rowCount > 0 && issuer.rowCount > 0) {
              let object = {
                issuerId: issuer.rows[0].id,
                issuer: issuer.rows[0].name,
                causeId: cause.rowCount > 0 ? cause.rows[0].id : null,
                cause: cause.rowCount > 0 ? cause.rows[0].name : cause,
                receiver: institution.rows[0].name,
                receiverId: donation.institution,
                description: donation.description,
                date: date,
                value: donation.value,
                type: "Donation",
              };
              donationList.push(object);
            }
          }
        }

        res.send(donationList);
      });
  });

  //obter pagamentos efetuados pelo utilizador
  app.get("/my-payments", async (req, res) => {
    let donationList = [];
    const endpoint = new URL("http://localhost:50005/my-payments");
    const params = {
      uuid: req.query.uuid,
    };
    console.log(req.query.uuid)

    endpoint.search = new URLSearchParams(params);
    fetch(endpoint)
      .then((data) => data.json())
      .then(async (data) => {
        if(Array.isArray(data)){
          for (let donation of data) {
            let cause;
            if(donation.cause == "" || donation.cause == "null"){
              console.log("asdasdasdasdasdasd")
              cause = ""
            } else {
              (cause = await pgPool.query(
                "SELECT * FROM causes WHERE id = $1",
                [donation.cause]
              ));
            }
            let institution = await pgPool.query(
              "SELECT id, name from institution WHERE corda_uuid = $1",
              [donation.issuer]
            );
  
            let company = await pgPool.query(
              "SELECT id, name FROM company WHERE corda_uuid = $1",
              [donation.receiver]
            );
            let dateArray = donation.date.split(" ");

            let date = new Date(
              Date.parse(
                dateArray[3] +
                  " " +
                  dateArray[1] +
                  " " +
                  dateArray[2] +
                  " " +
                  dateArray[5]
              )
            );
  
            if (institution.rowCount > 0 && company.rowCount > 0) {
              let object = {
                issuerId: institution.rows[0].id,
                issuer: institution.rows[0].name,
                causeId: cause.rowCount > 0 ? cause.rows[0].id : null,
                cause: cause.rowCount > 0 ? cause.rows[0].name : cause,
                receiverId: company.rows[0].id,
                receiver: company.rows[0].name,
                description: donation.description,
                date: date,
                value: donation.value,
                type: "Payment",
              };
              donationList.push(object);
            }
          }
        }
        
        res.send(donationList);
      });
  });

  //obter pagamentos recebidos pelo utilizador
  app.get("/received-payments", async (req, res) => {
    let donationList = [];
    const endpoint = new URL("http://localhost:50005/received-payments");
    const params = {
      uuid: req.query.uuid,
    };

    endpoint.search = new URLSearchParams(params);
    fetch(endpoint)
      .then((data) => data.json())
      .then(async (data) => {
        if(Array.isArray(data)){
          for (let donation of data) {
            let cause;
            donation.cause == ""
              ? (cause = "")
              : (cause = await pgPool.query(
                  "SELECT * FROM causes WHERE id = $1",
                  [donation.cause]
                ));
            let institution = await pgPool.query(
              "SELECT id, name from institution WHERE corda_uuid = $1",
              [donation.issuer]
            );
  
            let company = await pgPool.query(
              "SELECT id, name FROM company WHERE corda_uuid = $1",
              [donation.receiver]
            );
            let dateArray = donation.date.split(" ");
  
            let date = new Date(
              Date.parse(
                dateArray[3] +
                  " " +
                  dateArray[1] +
                  " " +
                  dateArray[2] +
                  " " +
                  dateArray[5]
              )
            );
  
            if (institution.rowCount > 0 && company.rowCount > 0) {
              let object = {
                issuerId: institution.rows[0].id,
                issuer: institution.rows[0].name,
                causeId: cause.rowCount > 0 ? cause.rows[0].id : null,
                cause: cause.rowCount > 0 ? cause.rows[0].name : cause,
                receiverId: company.rows[0].id,
                receiver: company.rows[0].name,
                description: donation.description,
                date: date,
                value: donation.value,
                type: "Payment",
              };
              donationList.push(object);
            }
          }
        }
        
        res.send(donationList);
      });
  });

  //obter informação das doações recebidas relacionadas com a causa específica
  app.get("/received-cause-donation", (req, res) => {
    let donationsList = [];
    pgPool.query(
      "SELECT * FROM causes WHERE id = $1",
      [req.query.causeId],
      (error, cause) => {
        if (!cause) {
          res.sendStatus(500);
        } else {
          //obtenção de todas as doações recebidas das instituição
          fetch(
            `http://localhost:50005/received-donations?uuid=${cause.rows[0].institutionid}`
          )
            .then((data) => data.json())
            .then(async (data) => {
              for (let donation of data) {
                if (donation.cause) {
                  //separação as doações que correspondem à causa pedida
                  if (donation.cause == cause.rows[0].id) {
                    let issuer;
                    issuer = await pgPool.query(
                      "SELECT name FROM users WHERE corda_uuid = $1",
                      [donation.issuer]
                    );
                    if (issuer.rowCount == 0) {
                      issuer = await pgPool.query(
                        "SELECT name FROM company WHERE corda_uuid = $1",
                        [donation.issuer]
                      );
                    }

                    let dateArray = donation.date.split(" ");

                    let date = new Date(
                      Date.parse(
                        dateArray[3] +
                          " " +
                          dateArray[1] +
                          " " +
                          dateArray[2] +
                          " " +
                          dateArray[5]
                      )
                    );

                    donation.date = date.toISOString().split("T")[0];
                    donation = {
                      ...donation,
                      time: date
                        .toISOString()
                        .split("T")[1]
                        .split("Z")[0]
                        .split(".")[0],
                      issuerInfo: issuer.rows[0].name,
                      type: "Donation",
                    };
                    donationsList.push(donation);
                  }
                }
              }
              res.send(donationsList);
            });
        }
      }
    );
  });
  //semelhante ao endpoint anterior
  app.get("/received-cause-payment", (req, res) => {
    let paymentList = [];
    pgPool.query(
      "SELECT * FROM causes WHERE id = $1",
      [req.query.causeId],
      (error, cause) => {
        if (!cause) {
          res.sendStatus(500);
        } else {
          fetch(
            `http://localhost:50005/my-payments?uuid=${cause.rows[0].institutionid}`
          )
            .then((data) => data.json())
            .then(async (data) => {
              for (let payment of data) {
                if (payment.cause) {
                  if (payment.cause == cause.rows[0].id) {
                    let receiver = await pgPool.query(
                      "SELECT name FROM company WHERE corda_uuid = $1",
                      [payment.receiver]
                    );

                    let dateArray = payment.date.split(" ");

                    let date = new Date(
                      Date.parse(
                        dateArray[3] +
                          " " +
                          dateArray[1] +
                          " " +
                          dateArray[2] +
                          " " +
                          dateArray[5]
                      )
                    );

                    payment.date = date.toISOString().split("T")[0];
                    payment = {
                      ...payment,
                      time: date
                        .toISOString()
                        .split("T")[1]
                        .split("Z")[0]
                        .split(".")[0],
                      issuerInfo: receiver.rows[0].name,
                      type: "Payment",
                    };
                    paymentList.push(payment);
                  }
                }
              }
              res.send(paymentList);
            });
        }
      }
    );
  });
  //registo de um pagamento, semelhante ao processo de criação de uma doação por parte de uma empresa
  app.post("/register-payment", verifySession(), async (req, res) => {
    // if (!req.body.issuer) {
    //   res.status(400).json("'error': 'Issuer not provided'");
    // }
    let sessionData = req.session.getAccessTokenPayload();
    if (!req.body.receiver) {
      res.status(400).json("'error': 'Receiver not provided'");
    }
    if (!req.body.amount) {
      res.status(400).json("'error': 'Amount not provided'");
    }
    let superTokensUserId = req.session.userId;

    pgPool.query(
      "SELECT id FROM users WHERE user_id = $1",
      [superTokensUserId],
      (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          pgPool.query(
            "SELECT * from user_institution WHERE user_id = $1 and institution_id = $2",
            [result.rows[0].id, sessionData.holderId],
            async (error, result) => {
              if (!result) {
                res.sendStatus(500);
              } else {
                if (result.rows[0].isadmin) {
                  fetch("http://localhost:50005/create-payment", {
                    method: "POST",
                    headers: {
                      "Content-type": "application/x-www-form-urlencoded",
                    },
                    body: new URLSearchParams({
                      value: req.body.amount,
                      issuer: sessionData.holderId,
                      receiver: req.body.receiver,
                      institution: sessionData.holderId,
                      cause: req.body.cause,
                      description: req.body.description
                        ? req.body.description
                        : "",
                    }),
                  })
                    .then((response) => {
                      return response.text();
                    })
                    .then((response) => {
                      res.sendStatus(200);
                    });
                } else {
                  let params = [
                    result.rows[0].user_id,
                    req.body.receiver,
                    req.body.amount,
                    req.body.description,
                    sessionData.holderId,
                  ];

                  if (req.body.cause) {
                    params = [...params, req.body.cause];
                  }
                  pgPool.query(
                    `INSERT INTO pending_payments(userid, receiver, amount, description, institutionid${
                      req.body.cause ? ",causeid" : ""
                    }) VALUES($1, $2, $3, $4, $5 ${
                      req.body.cause ? ",$6" : ""
                    })`,
                    params,
                    (error, result) => {
                      if (!result) {
                        console.log(error);
                        res.sendStatus(500);
                      } else {
                        res.sendStatus(200);
                      }
                    }
                  );
                }
              }
            }
          );
        }
      }
    );
  });
  //obter pagamentos pendentes para a instituição do utilizador que fez o pedido
  app.get("/pending-payments", verifySession(), (req, res) => {
    let paymentList = [];
    let sessionData = req.session.getAccessTokenPayload();
    if (!sessionData.hasInstitution) {
      res.send(400).json("'error': 'User does not belong to a Institution'");
    } else {
      pgPool.query(
        "SELECT * FROM pending_payments WHERE institutionid = $1",
        [sessionData.holderId],
        async (error, result) => {
          if (!result && result.rowCount == 0) {
            res.sendStatus(500);
          } else {
            for (let payment of result.rows) {
              let user = await pgPool.query(
                "SELECT name FROM users WHERE id = $1",
                [payment.userid]
              );
              let receiver = await pgPool.query(
                "SELECT name FROM company WHERE id = $1",
                [payment.receiver]
              );
              let cause;
              if (payment.causeid) {
                cause = await pgPool.query(
                  "SELECT * FROM causes WHERE id = $1",
                  [payment.causeid]
                );
              }

              if (user.rowCount == 0 && receiver.rowCount == 0) {
                res.sendStatus(500);
              } else {
                let paymentData = {
                  pending_paymentid: payment.id,
                  creatorName: user.rows[0].name,
                  creatorId: payment.userid,
                  receiverName: receiver.rows[0].name,
                  receiverId: payment.receiver,
                  causeName: cause ? cause.rows[0].name : "none",
                  causeId: payment.causeid,
                  amount: payment.amount,
                  description: payment.description,
                  institutionId: payment.institutionid,
                };
                paymentList.push(paymentData);
              }
            }
            res.send(paymentList);
          }
        }
      );
    }
  });
  //aceitação do pedido de pagamento, registando na blockchain e remoção do pedido pendente da base de dados
  app.post("/accept-institution-payment", verifySession(), (req, res) => {
    let sessionData = req.session.getAccessTokenPayload();
    pgPool.query(
      "SELECT * FROM pending_payments WHERE id = $1",
      [req.body.pending_paymentid],
      (error, result) => {
        if (!result) {
          console.log(error);
          res.sendStatus(500);
        } else {
          fetch("http://localhost:50005/create-payment", {
            method: "POST",
            body: new URLSearchParams({
              value: result.rows[0].amount,
              issuer: sessionData.holderId,
              receiver: result.rows[0].receiver,
              institution: sessionData.holderId,
              cause: result.rows[0].causeid,
              description: result.rows[0].description,
            }),
          })
            .then(() => {
              pgPool.query(
                "DELETE FROM pending_payments WHERE id = $1",
                [req.body.pending_paymentid],
                async (error, result) => {
                  if (!result) {
                    res.sendStatus(500);
                  } else {
                    res.sendStatus(200);
                  }
                }
              );
            })
            .catch(() => res.sendStatus(500));
        }
      }
    );
  });
  //rejeição do pedido de pagamento e remoção da base de dados
  app.post("/reject-institution-payment", verifySession(), (req, res) => {
    if (!req.body.pending_paymentid) {
      res.send(400).json("'error': 'No pending payment id provided'");
    } else {
      pgPool.query(
        "DELETE FROM pending_payments WHERE id = $1",
        [req.body.pending_paymentid],
        (error, result) => {
          if (!result) {
            console.log(error);
            res.sendStatus(500);
          } else {
            res.sendStatus(200);
          }
        }
      );
    }
  });
  app.get("/userInfo", verifySession(), async (req, res) => {
    let userId = req.session.userId;
    pgPool.query(
      "SELECT * FROM users WHERE user_id = $1",
      [userId],
      async (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          res.send(result.rows[0]);
        }
      }
    );
  });
  //criação de doação através do formulário
  app.post("/create-donation", verifySession(), async (req, res) => {
    let issuer_corda_uuid = await pgPool.query(
      "SELECT id FROM users WHERE user_id = $1",
      [req.session.userId]
    );

    let sessionData = req.session.getAccessTokenPayload();
    if (issuer_corda_uuid.rowCount > 0) {
      fetch("http://localhost:50005/create-donation", {
        method: "POST",
        body: new URLSearchParams({
          value: req.body.amount,
          issuer: sessionData.email,
          receiver: req.body.receiver,
          institution: req.body.receiver,
          cause: req.body.cause ? req.body.cause : "",
          description: req.body.description,
        }),
      }).then(res.sendStatus(200));
    } else {
      res.sendStatus(500);
    }
  });

  //criação de doação através do formulário que deverá ser associada ao utilizador anónimo
  app.post("/create-anon-donation", async (req, res) => {
    let anonUser = await Recipe.getUserByEmail("anonimo@anonimo.com");
    if (!anonUser) {
      res.sendStatus(500);
    } else {
      let issuer_corda_uuid = await pgPool.query(
        "SELECT id FROM users WHERE user_id = $1",
        [anonUser.id]
      );
      if (issuer_corda_uuid.rowCount > 0) {
        fetch("http://localhost:50005/create-donation", {
          method: "POST",
          body: new URLSearchParams({
            value: req.body.amount,
            issuer: anonUser.email,
            receiver: req.body.receiver,
            institution: req.body.receiver,
            cause: req.body.cause ? req.body.cause : "",
            description: req.body.description,
          }),
        }).then(res.sendStatus(200));
      } else {
        res.sendStatus(500);
      }
    }
  });
  //obter informação sobre instituições pendentes
  app.get("/pending-institutions", verifySession(), async (req, res) => {
    pgPool.query(
      "SELECT * FROM institution WHERE isaccepted = $1",
      [false],
      async (error, result) => {
        if (!result) {
          res.sendStatus(500);
        } else {
          let institutionList = [];
          for (let institution of result.rows) {
            let user = await pgPool.query(
              "SELECT user_id FROM user_institution WHERE institution_id = $1 AND isadmin = $2",
              [institution.id, true]
            );
            if (user.rowCount == 0) {
              res.sendStatus(500);
            } else {
              institution = {
                ...institution,
                user_id: user.rows[0].user_id,
              };
              institutionList.push(institution);
            }
          }
          res.send(institutionList);
        }
      }
    );
  });

  //obter os fundos de uma instituição, seria necessário caso os tokens do corda estivessem funcionais
  app.get("/get-institution-balance", verifySession(), async (req, res) => {
    let sessionData = req.session.getAccessTokenPayload();
    let institution_corda_uuid = await pgPool.query(
      "SELECT corda_uuid FROM institution WHERE id = $1",
      [sessionData.holderId]
    );

    if (institution_corda_uuid.rowCount > 0) {
      fetch(
        `http://localhost:50005/balance?uuid=${institution_corda_uuid.rows[0].corda_uuid}`
      )
        .then((data) => data.json())
        .then((data) => {
          return res.send(data);
        });
    } else {
      res.sendStatus(500);
    }
  });


  //obter saldo de um utizado especifico
  app.get("/get-user-balance", verifySession(), async (req, res) => {
    let sessionData = req.session.getAccessTokenPayload();
    fetch(`http://localhost:50005/balance?uuid=${sessionData.holderId}`)
      .then((data) => data.json())
      .then((data) => {
        return res.send(data);
      });
  });

  app.listen(PORT, HOST, () => {
    console.log(`Running on http://${HOST}:${PORT}`);
  });
};

app.use(errorHandler());

startServer();

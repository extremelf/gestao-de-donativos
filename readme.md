

<h4 align="center">Web platform to manage donations based on blockchain

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone https://gitlab.com/extremelf/gestao-de-donativos

# Go into the repository
$ cd Donativos

# Step 1 - Go into corda folder
$ cd Donativos-corda-kotlin

# Build nodes
$ ./gradlew clean deploynodes

# Run nodes
$ buid/nodes/runnodes

# Run api server after nodes running
$ ./gradlew runPartyAServer

# Step 2 - Go into Postgres-docker folder
$ cd Postgres-docker

# Start docker-compose
$ docker-compose up -d

# Create database 
$ execute database.sql

# Step 3 - Go into Supertokens-docker folder
$ cd SuperTokens-docker

# Start docker-compose
$ docker-compose up -d

# Step 4 - Go into node-backend folder
$ cd node-backend

# Install dependencies
$ npm i

# Start service
$ npm run start

# Step 5 - Go into Donativos-frontend-react folder
$ cd Donativos-frontend-react

# Go into auth folder
$ cd auth/

# Install dependencies
$ npm i

# Start service
$ npm run start

# Close browser window that opened

# Step 6 - Go into Donativos-frontend-angular folder
$ cd Donativos-frontend-angular

# Install dependencies
$ npm i

# Start service
$ npm run start 

# Done!
```

## Technologies



- Node.js
- Angular
- React
- Corda r3
- SuperTokens
- Docker
- Postresql
- Spring

> Luís Fernandes & Joel Silva

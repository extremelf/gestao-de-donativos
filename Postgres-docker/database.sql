CREATE DATABASE projeto3-db;
    CREATE SCHEMA [IF NOT EXISTS] public;
        create table if not exists institution
        (
            id          uuid    default uuid_generate_v4() not null
                constraint institution_pk
                    primary key,
            name        varchar(100)                       not null,
            description varchar(500),
            nif         integer                            not null,
            corda_uuid  uuid,
            isaccepted  boolean default false              not null
        );

        create unique index if not exists institution_id_uindex
            on institution (id);

        create unique index if not exists institution_nif_uindex
            on institution (nif);

        create table if not exists company
        (
            id          uuid    default uuid_generate_v4() not null
                constraint company_pk
                    primary key,
            name        varchar(100)                       not null,
            description varchar(500),
            nif         integer                            not null,
            corda_uuid  uuid,
            isaccepted  boolean default false              not null
        );

        create unique index if not exists company_id_uindex
            on company (id);

        create unique index if not exists company_nif_uindex
            on company (nif);

        create table if not exists users
        (
            id         uuid    default uuid_generate_v4() not null
                constraint users_pk
                    primary key,
            user_id    uuid,
            corda_uuid uuid,
            isadmin    boolean default false              not null,
            name       varchar(50),
            address    varchar(200),
            nif        integer
        );

        create table if not exists causes
        (
            id            uuid    default uuid_generate_v4() not null
                constraint causes_pk
                    primary key,
            name          varchar(100)                       not null,
            description   varchar(500),
            goal          integer                            not null,
            institutionid uuid                               not null
                constraint causes_institution_fk
                    references institution,
            isaccepted    boolean default false              not null,
            creator       uuid                               not null
                constraint causes_users_id_fk
                    references users,
            creationdate  date    default date(now())        not null
        );

        create unique index if not exists causes_id_uindex
            on causes (id);

        create unique index if not exists users_id_uindex
            on users (id);

        create unique index if not exists users_nif_uindex
            on users (nif);

        create table if not exists user_company
        (
            user_id    uuid                  not null
                constraint "user-company_users_id_fk"
                    references users,
            company_id uuid                  not null
                constraint "user-company_company_id_fk"
                    references company,
            isadmin    boolean default false not null,
            ispending  boolean default true  not null,
            constraint "user-company_pk"
                primary key (company_id, user_id)
        );

        create table if not exists user_institution
        (
            user_id        uuid                  not null
                constraint "user-institution_users_id_fk"
                    references users,
            institution_id uuid                  not null
                constraint "user-institution_institution_id_fk"
                    references institution,
            isadmin        boolean default false not null,
            ispending      boolean default true  not null,
            constraint "user-institution_pk"
                primary key (institution_id, user_id)
        );

        create unique index if not exists "user-institution_user_id_uindex"
            on user_institution (user_id);

        create table if not exists pending_payments
        (
            id            uuid default uuid_generate_v4() not null
                constraint pending_payments_pk
                    primary key,
            userid        uuid                            not null
                constraint pending_payments_users_id_fk
                    references users,
            receiver      uuid                            not null
                constraint pending_payments_company_id_fk
                    references company,
            amount        integer                         not null,
            description   varchar(500),
            institutionid uuid                            not null
                constraint pending_payments_institution_id_fk
                    references institution,
            causeid       uuid
        );

        create unique index if not exists pending_payments_id_uindex
            on pending_payments (id);

        create table if not exists pending_donations
        (
            id          uuid default uuid_generate_v4() not null
                constraint pending_donations_pk
                    primary key,
            userid      uuid                            not null
                constraint pending_donations_users_id_fk
                    references users,
            receiver    uuid                            not null
                constraint pending_donations_institution_id_fk
                    references institution,
            amount      integer                         not null,
            description varchar(500),
            companyid   uuid                            not null
                constraint pending_donations_company_id_fk
                    references company,
            causeid     uuid
                constraint pending_donations_causes_id_fk
                    references causes
        );

        create unique index if not exists pending_donations_id_uindex
            on pending_donations (id);

        create or replace function uuid_nil() returns uuid
            immutable
            strict
            parallel safe
            language c
        as
        $$
        begin
        -- missing source code
        end;
        $$;

        create or replace function uuid_ns_dns() returns uuid
            immutable
            strict
            parallel safe
            language c
        as
        $$
        begin
        -- missing source code
        end;
        $$;

        create or replace function uuid_ns_url() returns uuid
            immutable
            strict
            parallel safe
            language c
        as
        $$
        begin
        -- missing source code
        end;
        $$;

        create or replace function uuid_ns_oid() returns uuid
            immutable
            strict
            parallel safe
            language c
        as
        $$
        begin
        -- missing source code
        end;
        $$;

        create or replace function uuid_ns_x500() returns uuid
            immutable
            strict
            parallel safe
            language c
        as
        $$
        begin
        -- missing source code
        end;
        $$;

        create or replace function uuid_generate_v1() returns uuid
            strict
            parallel safe
            language c
        as
        $$
        begin
        -- missing source code
        end;
        $$;

        create or replace function uuid_generate_v1mc() returns uuid
            strict
            parallel safe
            language c
        as
        $$
        begin
        -- missing source code
        end;
        $$;

        create or replace function uuid_generate_v3(namespace uuid, name text) returns uuid
            immutable
            strict
            parallel safe
            language c
        as
        $$
        begin
        -- missing source code
        end;
        $$;

        create or replace function uuid_generate_v4() returns uuid
            strict
            parallel safe
            language c
        as
        $$
        begin
        -- missing source code
        end;
        $$;

        create or replace function uuid_generate_v5(namespace uuid, name text) returns uuid
            immutable
            strict
            parallel safe
            language c
        as
        $$
        begin
        -- missing source code
        end;
        $$;

